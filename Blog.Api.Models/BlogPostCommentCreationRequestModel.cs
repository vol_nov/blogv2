﻿namespace Blog.Api.Models
{
    public class BlogPostCommentCreationRequestModel
    {
        public string Content { get; set; }
    }
}