﻿using System;

namespace Blog.Api.Models
{
    public class BlogPostCommentCreationResponseModel
    {
        public Guid BlogPostCommentId { get; set; }

        public BlogPostCommentCreationResponseModel(Guid blogPostCommentId)
        {
            BlogPostCommentId = blogPostCommentId;
        }
    }
}