﻿using System;

namespace Blog.Api.Models
{
    public class BlogPostCommentResponseModel
    {
        public Guid BlogPostCommentId { get; }
        public Guid BlogPostId { get; }
        public string Content { get; }
        public int UserId { get; }
        public DateTimeOffset CreatedDate { get; }

        public BlogPostCommentResponseModel(
            Guid blogPostCommentId,
            Guid blogPostId,
            string content,
            int userId,
            DateTimeOffset createdDate)
        {
            BlogPostCommentId = blogPostCommentId;
            BlogPostId = blogPostId;
            Content = content;
            UserId = userId;
            CreatedDate = createdDate;
        }
    }
}