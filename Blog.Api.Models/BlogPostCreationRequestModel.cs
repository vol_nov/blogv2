﻿namespace Blog.Api.Models
{
    public class BlogPostCreationRequestModel
    {
        public string Header { get; set; }
        public string Description { get; set; }
    }
}