﻿using System;

namespace Blog.Api.Models
{
    public class BlogPostCreationResponseModel
    {
        public Guid BlogPostId { get; }

        public BlogPostCreationResponseModel(Guid blogPostId)
        {
            BlogPostId = blogPostId;
        }
    }
}