﻿using System;

namespace Blog.Api.Models
{
    public class BlogPostResponseModel
    {
        public Guid BlogPostId { get; }
        public string Header { get; }
        public string Description { get; }
        public DateTimeOffset CreatedDate { get; }
        public int AuthorId { get; }
        public int LikesCount { get; }
        public bool IsLikedByUser { get; }

        public BlogPostResponseModel(
            Guid blogPostId,
            string header,
            string description,
            DateTimeOffset createdDate,
            int authorId,
            int likesCount,
            bool isLikedByUser)
        {
            BlogPostId = blogPostId;
            Header = header;
            Description = description;
            CreatedDate = createdDate;
            AuthorId = authorId;
            LikesCount = likesCount;
            IsLikedByUser = isLikedByUser;
        }
    }
}