﻿namespace Blog.Api.Models
{
    public class CountryResponseModel
    {
        public int CountryId { get; }
        public string Name { get; }

        public CountryResponseModel(int countryId, string name)
        {
            CountryId = countryId;
            Name = name;
        }
    }
}