﻿namespace Blog.Api.Models
{
    public sealed class UserLoginResponseModel
    {
        public string Jwt { get; set; }
    }
}