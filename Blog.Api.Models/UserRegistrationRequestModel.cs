﻿using System;

namespace Blog.Api.Models
{
    public sealed class UserRegistrationRequestModel
    {
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int CountryId { get; set; }
    }
}