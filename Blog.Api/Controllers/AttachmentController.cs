﻿using System;
using System.Threading.Tasks;
using Blog.Api.Extensions;
using Blog.Core.Commands;
using Blog.Core.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Api.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}")]
    public class AttachmentController : ControllerBase
    {
        private readonly IMediator _mediator;

        [Authorize]
        [HttpGet("attachments/{attachmentId}")]
        public async Task<IActionResult> GetAttachment([FromRoute] Guid attachmentId)
        {
            var attachmentContentQuery = new AttachmentContentQuery(attachmentId);
            var result = await _mediator.Send(attachmentContentQuery);
            return File(result, "application/octet-stream");
        }

        [Authorize]
        [HttpGet("blogPosts/{blogPostId}")]
        public async Task<IActionResult> AddAttachments([FromRoute] Guid blogPostId)
        {
            var command = new AddBlogPostAttachmentsCommand(blogPostId, this.GetAttachedFiles());
            await _mediator.Send(command);
            return Accepted();
        }
    }
}