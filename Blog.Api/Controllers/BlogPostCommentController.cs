﻿using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Commands;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Core.Queries;
using Blog.Infrastructure.Authentication.Providers.Interfaces;

namespace Blog.Api.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}")]
    public sealed class BlogPostCommentController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IBlogPostCommentMapper _blogPostCommentMapper;
        private readonly ICurrentUserDataProvider _currentUserDataProvider;

        public BlogPostCommentController(
            IMediator mediator,
            IBlogPostCommentMapper blogPostCommentMapper,
            ICurrentUserDataProvider currentUserDataProvider)
        {
            _mediator = mediator;
            _blogPostCommentMapper = blogPostCommentMapper;
            _currentUserDataProvider = currentUserDataProvider;
        }

        [Authorize]
        [HttpGet("blogPosts/{blogPostId}/comments")]
        public async Task<IReadOnlyCollection<BlogPostCommentResponseModel>> GetComments(
            [FromBody] Guid blogPostId,
            [FromQuery] int page,
            [FromQuery] int size)
        {
            var blogPostCommentQuery = new BlogPostCommentsQuery(blogPostId, page, size);
            var result = await _mediator.Send(blogPostCommentQuery);
            return result.Select(_blogPostCommentMapper.MapToResponseModel).ToList();
        }

        [Authorize]
        [HttpPost("blogPosts/{blogPostId}/comment")]
        public async Task<BlogPostCommentCreationResponseModel> AddBlogPostComment(
            [FromRoute] Guid blogPostId,
            [FromBody] BlogPostCommentCreationRequestModel creationRequestModel)
        {
            var addBlogPostCommentCommand = new AddBlogPostCommentCommand(
                blogPostId,
                creationRequestModel?.Content,
                _currentUserDataProvider.Current.Id);

            var result = await _mediator.Send(addBlogPostCommentCommand);
            return _blogPostCommentMapper.MapToResponseModel(result);
        }
    }
}
