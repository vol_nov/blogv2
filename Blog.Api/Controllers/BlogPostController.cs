﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Queries;
using Blog.Infrastructure.Authentication.Providers.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Api.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}")]
    public class BlogPostController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IBlogPostMapper _blogPostMapper;
        private readonly ICurrentUserDataProvider _currentUserDataProvider;

        public BlogPostController(
            IMediator mediator,
            IBlogPostMapper blogPostMapper,
            ICurrentUserDataProvider currentUserDataProvider)
        {
            _mediator = mediator;
            _blogPostMapper = blogPostMapper;
            _currentUserDataProvider = currentUserDataProvider;
        }

        [Authorize]
        [HttpGet("blogPosts")]
        public async Task<IReadOnlyCollection<BlogPostResponseModel>> GetCurrentUserBlogPosts()
        {
            var blogPostsQuery = new BlogPostsQuery(_currentUserDataProvider.Current.Id, _currentUserDataProvider.Current.Id);
            var result = await _mediator.Send(blogPostsQuery);
            return _blogPostMapper.MapToResponseModel(result);
        }

        [Authorize]
        [HttpGet("users/{userId}/blogPosts")]
        public async Task<IReadOnlyCollection<BlogPostResponseModel>> GetUserBlogPosts([FromRoute] int userId)
        {
            var blogPostsQuery = new BlogPostsQuery(_currentUserDataProvider.Current.Id, userId);
            var result = await _mediator.Send(blogPostsQuery);
            return _blogPostMapper.MapToResponseModel(result);
        }

        [Authorize]
        [HttpPost("blogPosts")]
        public async Task<BlogPostCreationResponseModel> AddBlogPost([FromBody] BlogPostCreationRequestModel creationRequestModel)
        {
            var command = _blogPostMapper.MapToCommand(creationRequestModel, _currentUserDataProvider.Current.Id);
            var result = await _mediator.Send(command);
            return _blogPostMapper.MapToResponseModel(result);
        }
    }
}