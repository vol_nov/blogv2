﻿using System;
using System.Threading.Tasks;
using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Commands;
using Blog.Infrastructure.Authentication.Providers.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Api.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}")]
    public sealed class BlogPostLikeController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IBlogPostLikeMapper _blogPostLikeMapper;
        private readonly ICurrentUserDataProvider _currentUserDataProvider;

        public BlogPostLikeController(
            IMediator mediator,
            IBlogPostLikeMapper blogPostLikeMapper,
            ICurrentUserDataProvider currentUserDataProvider)
        {
            _mediator = mediator;
            _blogPostLikeMapper = blogPostLikeMapper;
            _currentUserDataProvider = currentUserDataProvider;
        }

        [Authorize]
        [HttpPost("blogPosts/{blogPostId}/like")]
        public async Task<BlogPostLikeResponseModel> AddBlogPostLike([FromRoute] Guid blogPostId)
        {
            var addOrRemoveBlogPostLikeCommand = new AddOrRemoveBlogPostLikeCommand(blogPostId, _currentUserDataProvider.Current.Id);
            var result = await _mediator.Send(addOrRemoveBlogPostLikeCommand);
            return _blogPostLikeMapper.MapToResponseModel(result);
        }
    }
}
