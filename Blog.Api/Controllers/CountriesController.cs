﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Api.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/countries")]
    public class CountriesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ICountryMapper _countryMapper;

        public CountriesController(IMediator mediator, ICountryMapper countryMapper)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _countryMapper = countryMapper ?? throw new ArgumentNullException(nameof(countryMapper));
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetCountries()
        {
            var result = await _mediator.Send(new CountriesQuery());
            var response = _countryMapper.MapToResponseModels(result);
            return Ok(response);
        }
    }
}