﻿using System;
using System.Threading.Tasks;
using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Api.Controllers
{
    [ApiController]
    [ApiVersion("1")]
    [Route("api/v{version:apiVersion}/users")]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IUserMapper _userMapper;

        public UserController(IMediator mediator, IUserMapper userMapper)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
            _userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<UserLoginResponseModel> Login([FromBody] UserLoginRequestModel requestModel)
        {
            var command = _userMapper.ToCommand(requestModel);
            var result = await _mediator.Send(command);
            var response = _userMapper.ToResponseModel(result);
            return response;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserRegistrationRequestModel requestModel)
        {
            var command = _userMapper.ToCommand(requestModel);
            await _mediator.Send(command);
            return Ok();
        }
    }
}