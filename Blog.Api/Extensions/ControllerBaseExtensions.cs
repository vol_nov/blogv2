﻿using Blog.Core.Commands;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Blog.Api.Extensions
{
    public static class ControllerBaseExtensions
    {
        public static IReadOnlyCollection<AttachedFile> GetAttachedFiles(this ControllerBase controller)
        {
            return controller
                .Request
                .Form
                .Files
                .Select(x => new AttachedFile(x.FileName, x.OpenReadStream()))
                .ToList();
        }
    }
}
