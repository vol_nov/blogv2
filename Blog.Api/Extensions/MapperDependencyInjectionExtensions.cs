﻿using Blog.Api.Mappers;
using Blog.Api.Mappers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Blog.Api.Extensions
{
    public static class MapperDependencyInjectionExtensions
    {
        public static IServiceCollection AddApiMappers(this IServiceCollection services)
        {
            services.AddScoped<IUserMapper, UserMapper>();
            services.AddScoped<ICountryMapper, CountryMapper>();
            services.AddScoped<IBlogPostLikeMapper, BlogPostLikeMapper>();
            services.AddScoped<IBlogPostCommentMapper, BlogPostCommentMapper>();
            services.AddScoped<IBlogPostMapper, BlogPostMapper>();

            return services;
        }
    }
}