﻿using Blog.Api.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Blog.Api.Extensions
{
    public static class MiddlewareApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionMiddleware(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<ExceptionMiddleware>();

            return applicationBuilder;
        }
    }
}