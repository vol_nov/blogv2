﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Blog.Api.Helpers
{
    public static class JsonHelper
    {
        private static readonly JsonSerializerOptions JsonSerializerOptions;

        static JsonHelper()
        {
            JsonSerializerOptions = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
        }

        public static string Serialize<T>(T input)
        {
            return JsonSerializer.Serialize(input, JsonSerializerOptions);
        }
    }
}