﻿using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Commands.Results;
using Blog.Core.Domain;

namespace Blog.Api.Mappers
{
    internal sealed class BlogPostCommentMapper : IBlogPostCommentMapper
    {
        public BlogPostCommentCreationResponseModel MapToResponseModel(AddBlogPostCommentCommandResult result)
        {
            return new BlogPostCommentCreationResponseModel(result.BlogPostCommentId);
        }

        public BlogPostCommentResponseModel MapToResponseModel(BlogPostComment comment)
        {
            return new BlogPostCommentResponseModel(
                comment.BlogPostCommentId,
                comment.BlogPostId,
                comment.Content,
                comment.UserId,
                comment.CreatedDate);
        }
    }
}