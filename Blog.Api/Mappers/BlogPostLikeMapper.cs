﻿using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Commands.Results;

namespace Blog.Api.Mappers
{
    internal sealed class BlogPostLikeMapper : IBlogPostLikeMapper
    {
        public BlogPostLikeResponseModel MapToResponseModel(AddOrRemoveBlogPostLikeCommandResult result)
        {
            return new BlogPostLikeResponseModel
            {
                IsLikeAdded = result.IsLikeAdded
            };
        }
    }
}