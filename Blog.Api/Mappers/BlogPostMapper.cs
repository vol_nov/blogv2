﻿using System.Collections.Generic;
using System.Linq;
using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;
using Blog.Core.Domain;

namespace Blog.Api.Mappers
{
    internal sealed class BlogPostMapper : IBlogPostMapper
    {
        public AddBlogPostCommand MapToCommand(BlogPostCreationRequestModel model, int currentUserId)
        {
            return new AddBlogPostCommand(model.Header, model.Description, currentUserId);
        }

        public BlogPostCreationResponseModel MapToResponseModel(AddBlogPostCommandResult result)
        {
            return new BlogPostCreationResponseModel(result.BlogPostId);
        }

        public IReadOnlyCollection<BlogPostResponseModel> MapToResponseModel(IReadOnlyCollection<BlogPost> blogPosts)
        {
            return blogPosts.Select(MapToResponseModel).ToList();
        }

        private BlogPostResponseModel MapToResponseModel(BlogPost blogPost)
        {
            return new BlogPostResponseModel(
                blogPost.BlogPostId,
                blogPost.Header,
                blogPost.Description,
                blogPost.CreatedDate,
                blogPost.AuthorId,
                blogPost.LikesCount,
                blogPost.IsLikedByUser);
        }
    }
}