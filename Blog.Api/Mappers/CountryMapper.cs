﻿using System.Collections.Generic;
using System.Linq;
using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Domain;

namespace Blog.Api.Mappers
{
    internal sealed class CountryMapper : ICountryMapper
    {
        public IReadOnlyCollection<CountryResponseModel> MapToResponseModels(IReadOnlyCollection<Country> countries)
        {
            return countries.Select(MapToResponseModel).ToList();
        }

        private CountryResponseModel MapToResponseModel(Country country)
        {
            return new CountryResponseModel(country.CountryId, country.Name);
        }
    }
}