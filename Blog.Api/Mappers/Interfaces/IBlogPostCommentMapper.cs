﻿using Blog.Api.Models;
using Blog.Core.Commands.Results;
using Blog.Core.Domain;

namespace Blog.Api.Mappers.Interfaces
{
    public interface IBlogPostCommentMapper
    {
        BlogPostCommentCreationResponseModel MapToResponseModel(AddBlogPostCommentCommandResult result);
        BlogPostCommentResponseModel MapToResponseModel(BlogPostComment comment);
    }
}