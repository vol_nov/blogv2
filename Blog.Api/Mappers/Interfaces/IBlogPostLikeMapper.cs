﻿using Blog.Api.Models;
using Blog.Core.Commands.Results;

namespace Blog.Api.Mappers.Interfaces
{
    public interface IBlogPostLikeMapper
    {
        BlogPostLikeResponseModel MapToResponseModel(AddOrRemoveBlogPostLikeCommandResult result);
    }
}