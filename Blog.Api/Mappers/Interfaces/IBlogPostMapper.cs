﻿using System.Collections.Generic;
using Blog.Api.Models;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;
using Blog.Core.Domain;

namespace Blog.Api.Mappers.Interfaces
{
    public interface IBlogPostMapper
    {
        AddBlogPostCommand MapToCommand(BlogPostCreationRequestModel model, int currentUserId);
        BlogPostCreationResponseModel MapToResponseModel(AddBlogPostCommandResult result);
        IReadOnlyCollection<BlogPostResponseModel> MapToResponseModel(IReadOnlyCollection<BlogPost> blogPosts);
    }
}