﻿using System.Collections.Generic;
using Blog.Api.Models;
using Blog.Core.Domain;

namespace Blog.Api.Mappers.Interfaces
{
    public interface ICountryMapper
    {
        IReadOnlyCollection<CountryResponseModel> MapToResponseModels(IReadOnlyCollection<Country> countries);
    }
}