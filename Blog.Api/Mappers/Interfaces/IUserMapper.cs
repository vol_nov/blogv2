﻿using Blog.Api.Models;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;

namespace Blog.Api.Mappers.Interfaces
{
    public interface IUserMapper
    {
        UserLoginCommand ToCommand(UserLoginRequestModel requestModel);
        UserRegistrationCommand ToCommand(UserRegistrationRequestModel requestModel);
        UserLoginResponseModel ToResponseModel(UserLoginCommandResult result);
    }
}