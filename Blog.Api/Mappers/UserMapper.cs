﻿using Blog.Api.Mappers.Interfaces;
using Blog.Api.Models;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;

namespace Blog.Api.Mappers
{
    internal sealed class UserMapper : IUserMapper
    {
        public UserLoginCommand ToCommand(UserLoginRequestModel requestModel)
        {
            return new UserLoginCommand
            {
                Email = requestModel.Email,
                Password = requestModel.Password,
            };
        }

        public UserRegistrationCommand ToCommand(UserRegistrationRequestModel requestModel)
        {
            return new UserRegistrationCommand()
            {
                Username = requestModel.Username,
                Email = requestModel.Email,
                DateOfBirth = requestModel.DateOfBirth,
                FullName = requestModel.FullName,
                Password = requestModel.Password,
                CountryId = requestModel.CountryId,
            };
        }

        public UserLoginResponseModel ToResponseModel(UserLoginCommandResult result)
        {
            return new UserLoginResponseModel
            {
                Jwt = result.Jwt,
            };
        }
    }
}