﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Blog.Api.Helpers;
using Blog.Api.Models;
using Blog.Core.Exceptions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Blog.Api.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext, ILogger<ExceptionMiddleware> logger)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ValidationException validationException)
            {
                await HandleExceptionAsync(
                    logger,
                    validationException,
                    httpContext,
                    HttpStatusCode.BadRequest,
                    string.Concat(validationException.Errors.Select(x => x.ErrorMessage)));
            }
            catch (UserNotExistsException userNotExistsException)
            {
                await HandleExceptionAsync(
                    logger,
                    userNotExistsException,
                    httpContext,
                    HttpStatusCode.Unauthorized);
            }
            catch (UserPasswordDontMatchException userPasswordDontMatchException)
            {
                await HandleExceptionAsync(
                    logger,
                    userPasswordDontMatchException,
                    httpContext,
                    HttpStatusCode.Unauthorized);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(
                    logger,
                    exception,
                    httpContext,
                    HttpStatusCode.InternalServerError,
                    "An unexpected error occurred while processing the request. Please try again or contact support.");
            }
        }

        private static Task HandleExceptionAsync(ILogger<ExceptionMiddleware> logger, Exception ex, HttpContext context, HttpStatusCode statusCode, string message = null)
        {
            logger.LogError(ex, "Exception occurred");

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;

            var response = new ErrorResponseModel
            {
                Message = message
            };

            var errorModelJson = JsonHelper.Serialize(response);

            return context.Response.WriteAsync(errorModelJson);
        }
    }
}