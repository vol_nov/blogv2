using Blog.Api.Extensions;
using Blog.Application;
using Blog.Core;
using Blog.Infrastructure.Authentication;
using Blog.Infrastructure.Configuration;
using Blog.Infrastructure.HealthChecks;
using Blog.Infrastructure.Logging;
using Blog.Infrastructure.Persistence.SqlServer;
using Blob.Infrastructure.Persistence.BlobStorage;
using Blog.Infrastructure.Swagger;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Blog.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddApiVersioning()
                .AddSwagger(JwtBearerDefaults.AuthenticationScheme)
                .AddControllers()
                .AddFluentValidation();

            services.AddSettingsDependencies(_configuration);
            services.AddApiMappers();
            services.AddCoreDependencies();
            services.AddApplicationDependencies();
            services.AddAuthenticationDependencies();
            services.AddSqlServerPersistenceDependencies();
            services.AddBlobStorageDependencies();
            services.AddLoggingDependencies();
            services.AddHealthCheckDependencies();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Blog v1"));
            }

            app.UseSerilogRequestLogging();

            app.UseHttpsRedirection();

            app.UseExceptionMiddleware();

            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            { 
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health", HealthCheckOptionsFactory.CreateOptions());
            });
        }
    }
}