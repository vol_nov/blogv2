﻿using System;
using System.Collections.Generic;
using Blog.Application.Handlers;
using Blog.Application.Mappers;
using Blog.Application.Mappers.Interfaces;
using Blog.Application.Pipelines;
using Blog.Application.Validators;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;
using Blog.Core.Domain;
using Blog.Core.Queries;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Blog.Application
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddApplicationDependencies(this IServiceCollection services)
        {
            services.AddMediatR();
            services.AddHandlers();
            services.AddValidators();
            services.AddMappers();

            return services;
        }

        private static IServiceCollection AddMediatR(this IServiceCollection services)
        {
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingPipelineBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationPipelineBehavior<,>));

            return services;
        }

        private static IServiceCollection AddHandlers(this IServiceCollection services)
        {
            services.AddScoped<IRequestHandler<UserLoginCommand, UserLoginCommandResult>, UserLoginCommandHandler>();
            services.AddScoped<IRequestHandler<UserRegistrationCommand>, UserRegistrationCommandHandler>();
            services.AddScoped<IRequestHandler<CountriesQuery, IReadOnlyCollection<Country>>, CountriesQueryHandler>();
            services.AddScoped<IRequestHandler<AddOrRemoveBlogPostLikeCommand, AddOrRemoveBlogPostLikeCommandResult>, AddOrRemoveBlogPostLikeCommandHandler>();
            services.AddScoped<IRequestHandler<AddBlogPostCommand, AddBlogPostCommandResult>, AddBlogPostCommandHandler>();
            services.AddScoped<IRequestHandler<BlogPostsQuery, IReadOnlyCollection<BlogPost>>, BlogPostsQueryHandler>();

            return services;
        }

        private static IServiceCollection AddValidators(this IServiceCollection services)
        {
            services.AddScoped<IValidator<UserLoginCommand>, UserLoginCommandValidator>();
            services.AddScoped<IValidator<UserRegistrationCommand>, UserRegistrationCommandValidator>();

            return services;
        }

        private static IServiceCollection AddMappers(this IServiceCollection services)
        {
            services.AddScoped<IUserMapper, UserMapper>();
            services.AddScoped<IBlogPostCommentMapper, BlogPostCommentMapper>();
            services.AddScoped<IBlogPostMapper, BlogPostMapper>();

            return services;
        }
    }
}