﻿using Blog.Core.Commands;
using Blog.Core.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Blog.Application.Handlers
{
    internal sealed class AddBlogPostAttachmentsCommandHandler : IRequestHandler<AddBlogPostAttachmentsCommand>
    {
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly IAttachmentContentRepository _attachmentContentRepository;

        public AddBlogPostAttachmentsCommandHandler(
            IAttachmentRepository attachmentRepository,
            IAttachmentContentRepository attachmentContentRepository)
        {
            _attachmentRepository = attachmentRepository;
            _attachmentContentRepository = attachmentContentRepository;
        }

        public async Task<Unit> Handle(AddBlogPostAttachmentsCommand request, CancellationToken cancellationToken)
        {
            foreach (var file in request.Files)
            {
                var attachmentId = await _attachmentRepository.AddAttachment(request.BlogPostId, file);
                await _attachmentContentRepository.AddContent(attachmentId, file.Content);
                file.Dispose();
            }

            return Unit.Value;
        }
    }
}
