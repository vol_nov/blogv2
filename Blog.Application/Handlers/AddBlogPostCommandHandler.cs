﻿using System.Threading;
using System.Threading.Tasks;
using Blog.Application.Mappers.Interfaces;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;
using Blog.Core.Interfaces;
using MediatR;

namespace Blog.Application.Handlers
{
    internal sealed class AddBlogPostCommandHandler : IRequestHandler<AddBlogPostCommand, AddBlogPostCommandResult>
    {
        private readonly IBlogPostRepository _blogPostRepository;
        private readonly IBlogPostMapper _blogPostMapper;
        private readonly IDateTimeProvider _dateTimeProvider;

        public AddBlogPostCommandHandler(
            IBlogPostRepository blogPostRepository,
            IBlogPostMapper blogPostMapper,
            IDateTimeProvider dateTimeProvider)
        {
            _blogPostRepository = blogPostRepository;
            _blogPostMapper = blogPostMapper;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<AddBlogPostCommandResult> Handle(AddBlogPostCommand request, CancellationToken cancellationToken)
        {
            var blogPost = _blogPostMapper.MapToDomain(
                request,
                _dateTimeProvider.Current);

            var blogPostId = await _blogPostRepository.Add(blogPost);
            return new AddBlogPostCommandResult(blogPostId);
        }
    }
}