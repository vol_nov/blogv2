﻿using System.Threading;
using System.Threading.Tasks;
using Blog.Application.Mappers.Interfaces;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Authentication.Providers.Interfaces;
using MediatR;

namespace Blog.Application.Handlers
{
    internal sealed class AddBlogPostCommentCommandHandler : IRequestHandler<AddBlogPostCommentCommand, AddBlogPostCommentCommandResult>
    {
        private readonly IBlogPostCommentRepository _blogPostCommentRepository;
        private readonly IBlogPostCommentMapper _blogPostCommentMapper;
        private readonly IDateTimeProvider _dateTimeProvider;

        public AddBlogPostCommentCommandHandler(
            IBlogPostCommentRepository blogPostCommentRepository,
            IBlogPostCommentMapper blogPostCommentMapper,
            IDateTimeProvider dateTimeProvider)
        {
            _blogPostCommentRepository = blogPostCommentRepository;
            _blogPostCommentMapper = blogPostCommentMapper;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<AddBlogPostCommentCommandResult> Handle(AddBlogPostCommentCommand request, CancellationToken cancellationToken)
        {
            var comment = _blogPostCommentMapper.MapToDomain(
                request,
                _dateTimeProvider.Current);

            var blogPostCommentId = await _blogPostCommentRepository.Add(comment);

            return new AddBlogPostCommentCommandResult
            {
                BlogPostCommentId = blogPostCommentId,
            };
        }
    }
}