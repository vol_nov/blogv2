﻿using System.Threading;
using System.Threading.Tasks;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Authentication.Providers.Interfaces;
using MediatR;

namespace Blog.Application.Handlers
{
    internal sealed class AddOrRemoveBlogPostLikeCommandHandler : IRequestHandler<AddOrRemoveBlogPostLikeCommand, AddOrRemoveBlogPostLikeCommandResult>
    {
        private readonly IBlogPostLikeRepository _blogPostLikeRepository;

        public AddOrRemoveBlogPostLikeCommandHandler(IBlogPostLikeRepository blogPostLikeRepository)
        {
            _blogPostLikeRepository = blogPostLikeRepository;
        }

        public async Task<AddOrRemoveBlogPostLikeCommandResult> Handle(AddOrRemoveBlogPostLikeCommand request, CancellationToken cancellationToken)
        {
            var isLikeAdded = await _blogPostLikeRepository.Like(request.BlogPostId, request.CurrentUserId);
            return new AddOrRemoveBlogPostLikeCommandResult(isLikeAdded);
        }
    }
}