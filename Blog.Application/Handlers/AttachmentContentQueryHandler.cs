﻿using Blog.Core.Interfaces;
using Blog.Core.Queries;
using MediatR;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Blog.Application.Handlers
{
    public sealed class AttachmentContentQueryHandler : IRequestHandler<AttachmentContentQuery, Stream>
    {
        private readonly IAttachmentContentRepository _attachmentContentRepository;

        public AttachmentContentQueryHandler(IAttachmentContentRepository attachmentContentRepository)
        {
            _attachmentContentRepository = attachmentContentRepository;
        }

        public async Task<Stream> Handle(AttachmentContentQuery request, CancellationToken cancellationToken)
        {
            return await _attachmentContentRepository.GetContent(request.AttachmentId);
        }
    }
}
