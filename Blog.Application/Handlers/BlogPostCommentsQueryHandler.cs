﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Core.Queries;
using MediatR;

namespace Blog.Application.Handlers
{
    public class BlogPostCommentsQueryHandler : IRequestHandler<BlogPostCommentsQuery, IReadOnlyCollection<BlogPostComment>>
    {
        private readonly IBlogPostCommentRepository _blogPostCommentRepository;

        public BlogPostCommentsQueryHandler(IBlogPostCommentRepository blogPostCommentRepository)
        {
            _blogPostCommentRepository = blogPostCommentRepository;
        }

        public Task<IReadOnlyCollection<BlogPostComment>> Handle(BlogPostCommentsQuery request, CancellationToken cancellationToken)
        {
            return _blogPostCommentRepository.GetComments(request.BlogPostId, request.Page, request.Size);
        }
    }
}