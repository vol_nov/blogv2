﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Core.Queries;
using MediatR;

namespace Blog.Application.Handlers
{
    internal sealed class BlogPostsQueryHandler : IRequestHandler<BlogPostsQuery, IReadOnlyCollection<BlogPost>>
    {
        private readonly IBlogPostRepository _blogPostRepository;

        public BlogPostsQueryHandler(IBlogPostRepository blogPostRepository)
        {
            _blogPostRepository = blogPostRepository;
        }

        public Task<IReadOnlyCollection<BlogPost>> Handle(BlogPostsQuery request, CancellationToken cancellationToken)
        {
            return _blogPostRepository.GetBlogPosts(request.CurrentUserId, request.TargetUserId);
        }
    }
}