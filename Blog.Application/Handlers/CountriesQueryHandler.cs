﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Core.Queries;
using MediatR;

namespace Blog.Application.Handlers
{
    internal sealed class CountriesQueryHandler : IRequestHandler<CountriesQuery, IReadOnlyCollection<Country>>
    {
        private readonly ICountryRepository _countryRepository;

        public CountriesQueryHandler(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository ?? throw new ArgumentNullException(nameof(countryRepository));
        }

        public Task<IReadOnlyCollection<Country>> Handle(CountriesQuery request, CancellationToken cancellationToken)
        {
            return _countryRepository.GetCountries();
        }
    }
}