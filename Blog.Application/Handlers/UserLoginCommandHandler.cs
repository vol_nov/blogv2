﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Blog.Core.Commands;
using Blog.Core.Commands.Results;
using Blog.Core.Exceptions;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Authentication.Services.Interfaces;
using FluentValidation;
using MediatR;

namespace Blog.Application.Handlers
{
    internal sealed class UserLoginCommandHandler : IRequestHandler<UserLoginCommand, UserLoginCommandResult>
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHashService _passwordHashService;
        private readonly IJwtTokenService _jwtTokenService;

        public UserLoginCommandHandler(
            IUserRepository userRepository,
            IPasswordHashService passwordHashService,
            IJwtTokenService jwtTokenService)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _passwordHashService = passwordHashService ?? throw new ArgumentNullException(nameof(passwordHashService));
            _jwtTokenService = jwtTokenService ?? throw new ArgumentNullException(nameof(jwtTokenService));
        }

        public async Task<UserLoginCommandResult> Handle(UserLoginCommand request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetUserByEmail(request.Email);

            if (user == null)
                throw new UserNotExistsException($"User with email {request.Email} doesn't exist");

            var passwordHash = _passwordHashService.CalculateHash(request.Password);
            if (passwordHash != user.PasswordHash)
                throw new UserPasswordDontMatchException("You've entered wrong password for the user. Please, try again!");

            var jwt = _jwtTokenService.Create(user);

            return new UserLoginCommandResult
            {
                Jwt = jwt,
            };
        }
    }
}