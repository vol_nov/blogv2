﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Blog.Application.Mappers.Interfaces;
using Blog.Core.Commands;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Authentication.Services.Interfaces;
using FluentValidation;
using MediatR;

namespace Blog.Application.Handlers
{
    internal sealed class UserRegistrationCommandHandler : IRequestHandler<UserRegistrationCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserMapper _userMapper;
        private readonly IPasswordHashService _passwordHashService;

        public UserRegistrationCommandHandler(
            IUserRepository userRepository,
            IUserMapper userMapper,
            IPasswordHashService passwordHashService)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
            _passwordHashService = passwordHashService ?? throw new ArgumentNullException(nameof(passwordHashService));
        }

        public async Task<Unit> Handle(UserRegistrationCommand request, CancellationToken cancellationToken)
        {
            var passwordHash = _passwordHashService.CalculateHash(request.Password);
            var user = _userMapper.Map(request, passwordHash);
            await _userRepository.Add(user);

            return Unit.Value;
        }
    }
}