﻿using System;
using Blog.Application.Mappers.Interfaces;
using Blog.Core.Commands;
using Blog.Core.Domain;
using Blog.Core.Interfaces;

namespace Blog.Application.Mappers
{
    internal sealed class BlogPostCommentMapper : IBlogPostCommentMapper
    {
        public BlogPostComment MapToDomain(AddBlogPostCommentCommand command, DateTimeOffset createdDate)
        {
            return new BlogPostComment
            {
                Content = command.Content,
                UserId = command.CurrentUserId,
                BlogPostId = command.BlogPostId,
                CreatedDate = createdDate,
            };
        }
    }
}