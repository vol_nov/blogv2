﻿using System;
using Blog.Application.Mappers.Interfaces;
using Blog.Core.Commands;
using Blog.Core.Domain;
using Blog.Core.Interfaces;

namespace Blog.Application.Mappers
{
    internal sealed class BlogPostMapper : IBlogPostMapper
    {
        public BlogPost MapToDomain(AddBlogPostCommand command, DateTimeOffset createdDate)
        {
            return new BlogPost
            {
                Header = command.Header,
                Description = command.Description,
                AuthorId = command.CurrentUserId,
                CreatedDate = createdDate,
            };
        }
    }
}