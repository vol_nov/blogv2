﻿using System;
using Blog.Core.Commands;
using Blog.Core.Domain;

namespace Blog.Application.Mappers.Interfaces
{
    internal interface IBlogPostCommentMapper
    {
        BlogPostComment MapToDomain(AddBlogPostCommentCommand command, DateTimeOffset createdDate);
    }
}