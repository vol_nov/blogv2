﻿using System;
using Blog.Core.Commands;
using Blog.Core.Domain;

namespace Blog.Application.Mappers.Interfaces
{
    internal interface IBlogPostMapper
    {
        BlogPost MapToDomain(AddBlogPostCommand command, DateTimeOffset createdDate);
    }
}