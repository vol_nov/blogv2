﻿using Blog.Core.Commands;
using Blog.Core.Domain;

namespace Blog.Application.Mappers.Interfaces
{
    internal interface IUserMapper
    {
        User Map(UserRegistrationCommand command, string passwordHash);
    }
}