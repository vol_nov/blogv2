﻿using Blog.Application.Mappers.Interfaces;
using Blog.Core.Commands;
using Blog.Core.Domain;

namespace Blog.Application.Mappers
{
    internal sealed class UserMapper : IUserMapper
    {
        public User Map(UserRegistrationCommand command, string passwordHash)
        {
            return new User
            {
                Email = command.Email,
                FullName = command.FullName,
                UserName = command.Username,
                DateOfBirth = command.DateOfBirth,
                CountryId = command.CountryId,
                PasswordHash = passwordHash,
                Role = "User"
            };
        }
    }
}