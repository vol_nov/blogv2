﻿using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Blog.Application.Pipelines
{
    internal sealed class LoggingPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<LoggingPipelineBehavior<TRequest, TResponse>> _logger;

        public LoggingPipelineBehavior(ILogger<LoggingPipelineBehavior<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var requestType = typeof(TRequest);
            var responseType = typeof(TResponse);

            _logger.LogInformation(
                "Starting processing request of type {@requestType} {@request}",
                requestType,
                request);

            var response = await next();

            _logger.LogInformation("Ended processing request {@requestType} with response {@responseType} {@response}",
                requestType,
                responseType,
                response);

            return response;
        }
    }
}
