﻿using Blog.Application.Validators.Extensions;
using Blog.Core.Commands;
using Blog.Core.Interfaces;
using FluentValidation;

namespace Blog.Application.Validators
{
    internal sealed class AddBlogPostAttachmentsCommandValidator : AbstractValidator<AddBlogPostAttachmentsCommand>
    {
        public AddBlogPostAttachmentsCommandValidator(IBlogPostRepository blogPostRepository)
        {
            RuleFor(x => x.BlogPostId)
                .MustContainBlogPostWithIdAsync(blogPostRepository);

            RuleFor(x => x.Files)
                .NotNull()
                .NotEmpty();
        }
    }
}
