﻿using Blog.Application.Validators.Extensions;
using Blog.Core.Commands;
using Blog.Core.Interfaces;
using FluentValidation;

namespace Blog.Application.Validators
{
    internal sealed class AddBlogPostCommandValidator : AbstractValidator<AddBlogPostCommand>
    {
        public AddBlogPostCommandValidator(IUserRepository userRepository)
        {
            RuleFor(x => x.Header).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();

            RuleFor(x => x.CurrentUserId)
                .MustContainUserWithIdAsync(userRepository);
        }
    }
}