﻿using Blog.Application.Validators.Extensions;
using Blog.Core.Commands;
using Blog.Core.Interfaces;
using FluentValidation;

namespace Blog.Application.Validators
{
    internal sealed class AddBlogPostCommentCommandValidator : AbstractValidator<AddBlogPostCommentCommand>
    {
        public AddBlogPostCommentCommandValidator(
            IBlogPostRepository blogPostRepository,
            IUserRepository userRepository)
        {
            RuleFor(x => x.Content).NotEmpty();
            RuleFor(x => x.CurrentUserId).MustContainUserWithIdAsync(userRepository);
            RuleFor(x => x.BlogPostId).MustContainBlogPostWithIdAsync(blogPostRepository);
        }
    }
}