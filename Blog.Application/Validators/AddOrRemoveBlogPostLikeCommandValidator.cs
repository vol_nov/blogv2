﻿using Blog.Application.Validators.Extensions;
using Blog.Core.Commands;
using Blog.Core.Interfaces;
using FluentValidation;

namespace Blog.Application.Validators
{
    internal sealed class AddOrRemoveBlogPostLikeCommandValidator : AbstractValidator<AddOrRemoveBlogPostLikeCommand>
    {
        public AddOrRemoveBlogPostLikeCommandValidator(
            IBlogPostRepository blogPostRepository,
            IUserRepository userRepository)
        {
            RuleFor(x => x.BlogPostId).MustContainBlogPostWithIdAsync(blogPostRepository);
            RuleFor(x => x.CurrentUserId).MustContainUserWithIdAsync(userRepository);
        }
    }
}