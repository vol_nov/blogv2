﻿using Blog.Core.Interfaces;
using Blog.Core.Queries;
using FluentValidation;

namespace Blog.Application.Validators
{
    internal sealed class AttachmentContentQueryValidator : AbstractValidator<AttachmentContentQuery>
    {
        public AttachmentContentQueryValidator(IAttachmentRepository attachmentRepository)
        {
            RuleFor(x => x.AttachmentId)
                .MustAsync((attachmentId, token) => attachmentRepository.Exists(attachmentId));
        }
    }
}
