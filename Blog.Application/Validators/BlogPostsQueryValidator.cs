﻿using Blog.Application.Validators.Extensions;
using Blog.Core.Interfaces;
using Blog.Core.Queries;
using FluentValidation;

namespace Blog.Application.Validators
{
    internal sealed class BlogPostsQueryValidator : AbstractValidator<BlogPostsQuery>
    {
        public BlogPostsQueryValidator(IUserRepository userRepository)
        {
            RuleFor(x => x.CurrentUserId).MustContainUserWithIdAsync(userRepository);
            RuleFor(x => x.TargetUserId).MustContainUserWithIdAsync(userRepository);
        }
    }
}