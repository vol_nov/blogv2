﻿using System;
using Blog.Core.Interfaces;
using FluentValidation;

namespace Blog.Application.Validators.Extensions
{
    public static class BlogPostValidationExtensions
    {
        public static IRuleBuilderOptions<TEntity, Guid> MustContainBlogPostWithIdAsync<TEntity>(this IRuleBuilder<TEntity, Guid> ruleBuilder, IBlogPostRepository blogPostRepository)
        {
            return ruleBuilder.MustAsync((blogPostId, cancellationToken) => blogPostRepository.ContainsBlogPostWithId(blogPostId));
        }
    }
}