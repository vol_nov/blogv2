﻿using Blog.Core.Interfaces;
using FluentValidation;

namespace Blog.Application.Validators.Extensions
{
    public static class UserValidationExtensions
    {
        public static IRuleBuilderOptions<TEntity, int> MustContainUserWithIdAsync<TEntity>(this IRuleBuilder<TEntity, int> ruleBuilder, IUserRepository userRepository)
        {
            return ruleBuilder.MustAsync((userId, cancellationToken) => userRepository.ContainsUserWithId(userId));
        }
    }
}