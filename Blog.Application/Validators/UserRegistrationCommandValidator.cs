﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Blog.Core.Commands;
using Blog.Core.Interfaces;
using FluentValidation;

namespace Blog.Application.Validators
{
    internal sealed class UserRegistrationCommandValidator : AbstractValidator<UserRegistrationCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly ICountryRepository _countryRepository;

        public UserRegistrationCommandValidator(IUserRepository userRepository, ICountryRepository countryRepository)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
            _countryRepository = countryRepository ?? throw new ArgumentNullException(nameof(countryRepository));

            RuleFor(x => x.Email)
                .EmailAddress()
                .NotEmpty()
                .MustAsync(NotContainUserWithEmail)
                .WithMessage("There is already user with such email.");

            RuleFor(x => x.FullName)
                .NotEmpty();

            RuleFor(x => x.Username)
                .NotEmpty()
                .MustAsync(NotContainUserWithUsername)
                .WithMessage("There is already user with such username.");

            RuleFor(x => x.DateOfBirth)
                .NotEmpty()
                .GreaterThan(new DateTime(1900, 1, 1))
                .LessThan(DateTime.UtcNow);

            RuleFor(x => x.Password)
                .NotEmpty()
                .MinimumLength(8)
                .MaximumLength(128)
                .Must(ContainAlphaNumericAndSpecialCharacters)
                .WithMessage("Password must contain alpha-numeric and special characters.");

            RuleFor(x => x.CountryId)
                .GreaterThan(0)
                .MustAsync(BeValidCountry);
        }

        private async Task<bool> NotContainUserWithEmail(string email, CancellationToken cancellationToken)
        {
            return !(await _userRepository.ContainsUserWithEmail(email));
        }

        private async Task<bool> NotContainUserWithUsername(string email, CancellationToken cancellationToken)
        {
            return !(await _userRepository.ContainsUserWithUsername(email));
        }

        private async Task<bool> BeValidCountry(int countryId, CancellationToken cancellationToken)
        {
            return await _countryRepository.IsValidCountryId(countryId);
        }

        private static bool ContainAlphaNumericAndSpecialCharacters(string input)
        {
            bool containsNumeric = false, containsLetter = false, containsSpecialChar = false;

            foreach (var c in input)
            {
                if (char.IsLetter(c))
                    containsLetter = true;
                if (char.IsDigit(c))
                    containsNumeric = true;
                if (!char.IsLetterOrDigit(c))
                    containsSpecialChar = true;
            }

            return containsLetter && containsNumeric && containsSpecialChar;
        }
    }
}