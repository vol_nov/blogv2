﻿using MediatR;
using System;
using System.Collections.Generic;

namespace Blog.Core.Commands
{
    public class AddBlogPostAttachmentsCommand : IRequest
    {
        public Guid BlogPostId { get; }
        public IReadOnlyCollection<AttachedFile> Files { get; } 

        public AddBlogPostAttachmentsCommand(Guid blogPostId, IReadOnlyCollection<AttachedFile> files)
        {
            BlogPostId = blogPostId;
            Files = files;
        }
    }
}
