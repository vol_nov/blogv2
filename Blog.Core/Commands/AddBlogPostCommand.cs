﻿using Blog.Core.Commands.Results;
using MediatR;

namespace Blog.Core.Commands
{
    public class AddBlogPostCommand : IRequest<AddBlogPostCommandResult>
    {
        public string Header { get; }
        public string Description { get; }
        public int CurrentUserId { get; }

        public AddBlogPostCommand(string header, string description, int currentUserId)
        {
            Header = header;
            Description = description;
            CurrentUserId = currentUserId;
        }
    }
}