﻿using System;
using Blog.Core.Commands.Results;
using MediatR;

namespace Blog.Core.Commands
{
    public class AddBlogPostCommentCommand : IRequest<AddBlogPostCommentCommandResult>
    {
        public Guid BlogPostId { get; }
        public string Content { get; }
        public int CurrentUserId { get; }

        public AddBlogPostCommentCommand(Guid blogPostId, string content, int currentUserId)
        {
            BlogPostId = blogPostId;
            Content = content;
            CurrentUserId = currentUserId;
        }
    }
}