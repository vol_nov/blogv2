﻿using System;
using Blog.Core.Commands.Results;
using MediatR;

namespace Blog.Core.Commands
{
    public class AddOrRemoveBlogPostLikeCommand : IRequest<AddOrRemoveBlogPostLikeCommandResult>
    {
        public Guid BlogPostId { get; }
        public int CurrentUserId { get; }

        public AddOrRemoveBlogPostLikeCommand(Guid blogPostId, int currentUserId)
        {
            BlogPostId = blogPostId;
            CurrentUserId = currentUserId;
        }
    }
}