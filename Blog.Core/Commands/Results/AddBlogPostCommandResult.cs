﻿using System;

namespace Blog.Core.Commands.Results
{
    public class AddBlogPostCommandResult
    {
        public Guid BlogPostId { get; }

        public AddBlogPostCommandResult(Guid blogPostId)
        {
            BlogPostId = blogPostId;
        }
    }
}