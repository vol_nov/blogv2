﻿using System;

namespace Blog.Core.Commands.Results
{
    public class AddBlogPostCommentCommandResult
    {
        public Guid BlogPostCommentId { get; set; }
    }
}