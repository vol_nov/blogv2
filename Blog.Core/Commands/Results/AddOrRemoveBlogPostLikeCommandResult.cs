﻿namespace Blog.Core.Commands.Results
{
    public class AddOrRemoveBlogPostLikeCommandResult
    {
        public bool IsLikeAdded { get; }

        public AddOrRemoveBlogPostLikeCommandResult(bool isLikeAdded)
        {
            IsLikeAdded = isLikeAdded;
        }
    }
}