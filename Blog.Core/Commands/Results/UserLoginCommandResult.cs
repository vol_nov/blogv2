﻿using Destructurama.Attributed;

namespace Blog.Core.Commands.Results
{
    public class UserLoginCommandResult
    {
        [NotLogged]
        public string Jwt { get; set; }
    }
}