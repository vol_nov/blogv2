﻿using Blog.Core.Commands.Results;
using Destructurama.Attributed;
using MediatR;
using Serilog;

namespace Blog.Core.Commands
{
    public class UserLoginCommand : IRequest<UserLoginCommandResult>
    {
        public string Email { get; set; }

        [NotLogged]
        public string Password { get; set; }
    }
}