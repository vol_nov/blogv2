﻿using System;
using Destructurama.Attributed;
using MediatR;

namespace Blog.Core.Commands
{
    public sealed class UserRegistrationCommand : IRequest
    {
        public string Email { get; set; }
        public string Username { get; set; }

        [NotLogged]
        public string Password { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int CountryId { get; set; }
    }
}