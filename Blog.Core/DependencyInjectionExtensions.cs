﻿using System;
using Blog.Core.Interfaces;
using Blog.Core.Providers;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Blog.Core
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddCoreDependencies(this IServiceCollection services)
        {
            services.AddScoped<IDateTimeProvider, DateTimeProvider>();

            return services;
        }
    }
}