﻿using System;

namespace Blog.Core.Domain
{
    public class BlogPost
    {
        public Guid BlogPostId { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public int AuthorId { get; set; }
        public bool IsLikedByUser { get; set; }
        public int LikesCount { get; set; }
    }
}