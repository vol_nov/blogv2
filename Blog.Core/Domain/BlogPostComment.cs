﻿using System;

namespace Blog.Core.Domain
{
    public class BlogPostComment
    {
        public Guid BlogPostCommentId { get; set; }
        public Guid BlogPostId { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }
}