﻿namespace Blog.Core.Domain
{
    public class Country
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
    }
}