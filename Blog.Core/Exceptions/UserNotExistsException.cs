﻿using System;

namespace Blog.Core.Exceptions
{
    public class UserNotExistsException : Exception
    {
        public UserNotExistsException(string message) : base(message)
        {
        }
    }
}