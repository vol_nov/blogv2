﻿using System;

namespace Blog.Core.Exceptions
{
    public class UserPasswordDontMatchException : Exception
    {
        public UserPasswordDontMatchException(string message) : base(message)
        {
        }
    }
}