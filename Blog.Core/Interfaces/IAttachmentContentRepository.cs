﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Blog.Core.Interfaces
{
    public interface IAttachmentContentRepository
    {
        Task AddContent(Guid attachmentId, Stream content);
        Task<Stream> GetContent(Guid attachmentId);
    }
}