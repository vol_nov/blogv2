﻿using Blog.Core.Commands;
using System;
using System.Threading.Tasks;

namespace Blog.Core.Interfaces
{
    public interface IAttachmentRepository
    {
        Task<bool> Exists(Guid attachmentId);
        Task<Guid> AddAttachment(Guid blogPostId, AttachedFile file);
    }
}
