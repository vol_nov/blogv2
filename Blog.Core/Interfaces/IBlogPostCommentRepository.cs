﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blog.Core.Domain;

namespace Blog.Core.Interfaces
{
    public interface IBlogPostCommentRepository
    {
        Task<Guid> Add(BlogPostComment comment);
        Task<IReadOnlyCollection<BlogPostComment>> GetComments(Guid blogPostId, int page, int size);
    }
}