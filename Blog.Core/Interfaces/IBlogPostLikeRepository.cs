﻿using System;
using System.Threading.Tasks;

namespace Blog.Core.Interfaces
{
    public interface IBlogPostLikeRepository
    {
        Task<bool> Like(Guid blogPostId, int userId);
    }
}