﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blog.Core.Domain;

namespace Blog.Core.Interfaces
{
    public interface IBlogPostRepository
    {
        Task<Guid> Add(BlogPost blogPost);
        Task<IReadOnlyCollection<BlogPost>> GetBlogPosts(int currentUserId, int targetUserId);
        Task<bool> ContainsBlogPostWithId(Guid blogPostId);
    }
}