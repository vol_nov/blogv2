﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Blog.Core.Domain;

namespace Blog.Core.Interfaces
{
    public interface ICountryRepository
    {
        Task<bool> IsValidCountryId(int countryId);
        Task<IReadOnlyCollection<Country>> GetCountries();
    }
}