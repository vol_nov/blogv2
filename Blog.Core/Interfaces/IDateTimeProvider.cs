﻿using System;

namespace Blog.Core.Interfaces
{
    public interface IDateTimeProvider
    {
        DateTimeOffset Current { get; }
    }
}