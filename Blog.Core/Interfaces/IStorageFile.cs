﻿namespace Blog.Core.Interfaces
{
    public interface IStorageFile
    {
        string Path();
    }
}