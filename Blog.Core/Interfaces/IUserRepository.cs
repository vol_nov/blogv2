﻿using System.Threading.Tasks;
using Blog.Core.Domain;

namespace Blog.Core.Interfaces
{
    public interface IUserRepository
    {
        Task<bool> ContainsUserWithId(int userId);
        Task<bool> ContainsUserWithEmail(string email);
        Task<bool> ContainsUserWithUsername(string userName);
        Task<User> GetUserByEmail(string email);
        Task Add(User user);
    }
}