﻿using System;
using Blog.Core.Interfaces;

namespace Blog.Core.Providers
{
    internal sealed class DateTimeProvider : IDateTimeProvider
    {
        public DateTimeOffset Current => DateTimeOffset.UtcNow;
    }
}