﻿using MediatR;
using System;
using System.IO;

namespace Blog.Core.Queries
{
    public class AttachmentContentQuery : IRequest<Stream>
    {
        public Guid AttachmentId { get; }

        public AttachmentContentQuery(Guid attachmentId)
        {
            AttachmentId = attachmentId;
        }
    }
}
