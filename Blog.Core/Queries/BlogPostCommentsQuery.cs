﻿using System;
using System.Collections.Generic;
using Blog.Core.Domain;
using MediatR;

namespace Blog.Core.Queries
{
    public class BlogPostCommentsQuery : IRequest<IReadOnlyCollection<BlogPostComment>>
    {
        public Guid BlogPostId { get; }
        public int Page { get; }
        public int Size { get; }

        public BlogPostCommentsQuery(Guid blogPostId, int page, int size)
        {
            BlogPostId = blogPostId;
            Page = page;
            Size = size;
        }
    }
}