﻿using System.Collections.Generic;
using Blog.Core.Domain;
using MediatR;

namespace Blog.Core.Queries
{
    public class BlogPostsQuery : IRequest<IReadOnlyCollection<BlogPost>>
    {
        public int CurrentUserId { get; }
        public int TargetUserId { get; }

        public BlogPostsQuery(int currentUserId, int targetUserId)
        {
            CurrentUserId = currentUserId;
            TargetUserId = targetUserId;
        }
    }
}