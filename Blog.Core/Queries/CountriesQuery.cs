﻿using System.Collections.Generic;
using Blog.Core.Domain;
using MediatR;

namespace Blog.Core.Queries
{
    public class CountriesQuery : IRequest<IReadOnlyCollection<Country>>
    {
    }
}