﻿using System;
using System.IO;

namespace Blog.Core.Commands
{
    public class AttachedFile : IDisposable
    {
        public string FileName { get; }
        public Stream Content { get; }

        public AttachedFile(string fileName, Stream content)
        {
            FileName = fileName;
            Content = content;
        }

        public void Dispose()
        {
            Content?.Dispose();
        }
    }
}
