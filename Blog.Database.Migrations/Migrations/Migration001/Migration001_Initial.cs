﻿using FluentMigrator;
using System.Collections.Generic;
using Blog.Database.Migrations.Migrations.Migration001.Schemas;

namespace Blog.Database.Migrations.Migrations.Migration001
{
    [Migration(1)]
    public class Migration001_Initial : ForwardOnlyMigration
    {
        public override void Up()
        {
            var dboSchema = new DboSchema();

            CreateSchema(dboSchema.Name);
            CreateSqlObjects(dboSchema.ScriptNames);
        }

        private void CreateSchema(string schemaName)
        {
            var createSchemaScript = GetSchemaCreationScript(schemaName);
            Execute.Sql(createSchemaScript);
        }

        private void CreateSqlObjects(IReadOnlyCollection<string> scriptNames)
        {
            foreach (var scriptName in scriptNames)
            {
                Execute.EmbeddedScript(scriptName);
            }
        }

        private static string GetSchemaCreationScript(string schemaName)
        {
            return $@"
                IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = '{schemaName}')
                BEGIN
                    EXEC('CREATE SCHEMA [{schemaName}]')
                END";
        }
    }
}
