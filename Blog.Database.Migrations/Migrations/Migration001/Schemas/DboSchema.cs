﻿using System.Collections.Generic;
using System.Linq;

namespace Blog.Database.Migrations.Migrations.Migration001.Schemas
{
    public class DboSchema : IDbSchema
    {
        public string Name => "dbo";

        private readonly IReadOnlyCollection<string> _tables = new[]
        {
            "Countries",
            "Users",
            "Attachments",
            "BlogPosts",
            "BlogPostLikes",
            "BlogPostComments",
            "BlogPostAttachments"
        };

        public IReadOnlyCollection<string> ScriptNames => _tables.Select(x => $"{Name}.tables.{x}.sql").ToList();
    }
}
