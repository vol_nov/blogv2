﻿using System.Collections.Generic;

namespace Blog.Database.Migrations.Migrations.Migration001.Schemas
{
    public interface IDbSchema
    {
        string Name { get; }
        IReadOnlyCollection<string> ScriptNames { get; }
    }
}
