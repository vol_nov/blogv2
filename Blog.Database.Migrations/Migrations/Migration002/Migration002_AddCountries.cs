﻿using FluentMigrator;

namespace Blog.Database.Migrations.Migrations.Migration002
{
    [Migration(2)]
    public class Migration002_AddCountries : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.EmbeddedScript($"{nameof(Migration002_AddCountries)}.sql");
        }
    }
}