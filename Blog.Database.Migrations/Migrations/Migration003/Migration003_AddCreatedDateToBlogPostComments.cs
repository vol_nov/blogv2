﻿using FluentMigrator;

namespace Blog.Database.Migrations.Migrations.Migration003
{
    [Migration(3)]
    public class Migration003_AddCreatedDateToBlogPostComments : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.EmbeddedScript($"{nameof(Migration003_AddCreatedDateToBlogPostComments)}.sql");
        }
    }
}