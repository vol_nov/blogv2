﻿using FluentMigrator;

namespace Blog.Database.Migrations.Migrations.Migration004
{
    [Migration(4)]
    public class Migration004_RemoveAttachmentBlogPostTableAndAddBlogPostIdToAttachment : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.EmbeddedScript($"{nameof(Migration004_RemoveAttachmentBlogPostTableAndAddBlogPostIdToAttachment)}.sql");
        }
    }
}
