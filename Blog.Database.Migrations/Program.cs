﻿using Blog.Database.Migrations.Migrations.Migration001;
using Blog.Database.Migrations.Settings;
using FluentMigrator.Runner;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Blog.Database.Migrations
{
    class Program
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", false, false)
                .Build();

            var dbSettings = configuration.GetSection(nameof(SqlServerSettings)).Get<SqlServerSettings>();

            var serviceProvider = new ServiceCollection()
                .AddFluentMigratorCore()
                .ConfigureRunner(x => x
                    .AddSqlServer()
                    .WithGlobalConnectionString(dbSettings.ConnectionString)
                    .ScanIn(typeof(Migration001_Initial).Assembly))
                .AddLogging(x => x.AddFluentMigratorConsole())
                .BuildServiceProvider();

            var migrationRunner = serviceProvider.GetService<IMigrationRunner>();
            migrationRunner.MigrateUp();
        }
    }
}
