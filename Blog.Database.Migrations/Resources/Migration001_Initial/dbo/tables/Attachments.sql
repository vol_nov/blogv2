﻿CREATE TABLE [Attachments]
(
    [AttachmentId] UNIQUEIDENTIFIER DEFAULT NEWSEQUENTIALID(),
    [BlobFileId] UNIQUEIDENTIFIER NOT NULL,
    [OriginalFileName] NVARCHAR(255) NOT NULL,
    [CreatedDate] DATETIMEOFFSET(7) NOT NULL,

    CONSTRAINT [PK_Attachments_AttachmentId] PRIMARY KEY ([AttachmentId]),
)
GO