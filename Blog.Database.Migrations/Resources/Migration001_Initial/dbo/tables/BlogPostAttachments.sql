﻿CREATE TABLE [BlogPostAttachments]
(
    [BlogPostAttachmentId] UNIQUEIDENTIFIER DEFAULT NEWSEQUENTIALID(),
    [BlogPostId] UNIQUEIDENTIFIER NOT NULL,
    [AttachmentId] UNIQUEIDENTIFIER NOT NULL,

    CONSTRAINT [PK_BlogPostAttachments_BlogPostLikeId] PRIMARY KEY ([BlogPostAttachmentId]),
    CONSTRAINT [FK_BlogPostAttachments_BlogPostId] FOREIGN KEY ([BlogPostId]) REFERENCES [dbo].[BlogPosts]([BlogPostId]),
    CONSTRAINT [FK_BlogPostAttachments_AttachmentId] FOREIGN KEY ([AttachmentId]) REFERENCES [dbo].[Attachments]([AttachmentId]),
)
GO