﻿CREATE TABLE [BlogPostComments]
(
    [BlogPostCommentId] UNIQUEIDENTIFIER DEFAULT NEWSEQUENTIALID(),
    [BlogPostId] UNIQUEIDENTIFIER NOT NULL,
    [UserId] INT NOT NULL,
    [Content] NVARCHAR(MAX) NOT NULL,

    CONSTRAINT [PK_BlogPostComments_BlogPostCommentId] PRIMARY KEY ([BlogPostCommentId]),
    CONSTRAINT [FK_BlogPostComments_BlogPostId] FOREIGN KEY ([BlogPostId]) REFERENCES [dbo].[BlogPosts]([BlogPostId]),
    CONSTRAINT [FK_BlogPostComments_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([UserId]),
)
GO