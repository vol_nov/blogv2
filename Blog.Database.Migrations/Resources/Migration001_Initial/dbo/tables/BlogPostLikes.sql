﻿CREATE TABLE [BlogPostLikes]
(
    [BlogPostLikeId] UNIQUEIDENTIFIER DEFAULT NEWSEQUENTIALID(),
    [BlogPostId] UNIQUEIDENTIFIER NOT NULL,
    [UserId] INT NOT NULL,

    CONSTRAINT [PK_BlogPostLikes_BlogPostLikeId] PRIMARY KEY ([BlogPostLikeId]),
    CONSTRAINT [FK_BlogPostLikes_BlogPostId] FOREIGN KEY ([BlogPostId]) REFERENCES [dbo].[BlogPosts]([BlogPostId]),
    CONSTRAINT [FK_BlogPostLikes_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users]([UserId]),
)
GO