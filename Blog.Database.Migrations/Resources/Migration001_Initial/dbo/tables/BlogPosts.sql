CREATE TABLE [dbo].[BlogPosts]
(
    [BlogPostId] UNIQUEIDENTIFIER DEFAULT NEWSEQUENTIALID(),
    [Header] NVARCHAR(MAX) NOT NULL,
    [Description] NVARCHAR(MAX) NOT NULL,
    [CreatedDate] DATETIMEOFFSET(7) NOT NULL,
    [AuthorId] INT NOT NULL,
    
    CONSTRAINT [PK_BlogPosts_BlogPostId] PRIMARY KEY ([BlogPostId]),
    CONSTRAINT [FK_BlogPosts_AuthorId] FOREIGN KEY ([AuthorId]) REFERENCES [dbo].[Users]([UserId])
)
GO