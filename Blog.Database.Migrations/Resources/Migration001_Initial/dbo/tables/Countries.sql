﻿CREATE TABLE [dbo].[Countries]
(
    [CountryId] INT IDENTITY(23, 1),
    [Name] VARCHAR(255) NOT NULL,

    CONSTRAINT [PK_Countries_CountryId] PRIMARY KEY ([CountryId]),
    CONSTRAINT [UQ_Countries_Name] UNIQUE ([CountryId])
)
GO