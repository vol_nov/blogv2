CREATE TABLE [dbo].[Users]
(
    [UserId] INT IDENTITY(1230, 1),
    [Email] VARCHAR(100) NOT NULL,
    [Username] VARCHAR(255) NOT NULL,
    [PasswordHash] VARCHAR(128) NOT NULL,
    [FullName] NVARCHAR(100) NOT NULL,
    [DateOfBirth] DATE NOT NULL,
    [CountryId] INT NOT NULL,

    CONSTRAINT PK_Users_UserId PRIMARY KEY ([UserId]),
    CONSTRAINT UQ_Users_Email UNIQUE ([Email]),
    CONSTRAINT UQ_Users_Username UNIQUE ([Email]),
    CONSTRAINT FK_Users_CountryId FOREIGN KEY ([CountryId]) REFERENCES dbo.Countries([CountryId])
)
GO