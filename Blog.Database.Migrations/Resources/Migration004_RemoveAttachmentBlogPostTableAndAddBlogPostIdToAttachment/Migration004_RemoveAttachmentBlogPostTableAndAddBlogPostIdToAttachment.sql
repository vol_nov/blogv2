﻿DROP TABLE [dbo].[BlogPostAttachments]
GO

ALTER TABLE [dbo].[Attachments] DROP COLUMN [BlobFileId]
GO

ALTER TABLE [dbo].[Attachments] ADD [BlogPostId] UNIQUEIDENTIFIER NOT NULL
CONSTRAINT [FK_Attachments_BlogPostId] FOREIGN KEY ([BlogPostId]) 
REFERENCES [BlogPosts] ([BlogPostId])