﻿namespace Blog.Database.Migrations.Settings
{
    internal sealed class SqlServerSettings
    {
        public string ConnectionString { get; set; }
    }
}
