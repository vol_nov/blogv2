﻿namespace Blog.Infrastructure.Authentication.Constants
{
    internal static class ClaimsConstants
    {
        public const string Email = nameof(Email); 
        public const string Id = nameof(Id); 
        public const string FullName = nameof(FullName); 
        public const string Role = nameof(Role); 
    }
}