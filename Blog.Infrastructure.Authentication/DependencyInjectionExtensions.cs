﻿using System.Text;
using Blog.Infrastructure.Authentication.Factories;
using Blog.Infrastructure.Authentication.Factories.Interfaces;
using Blog.Infrastructure.Authentication.Providers;
using Blog.Infrastructure.Authentication.Providers.Interfaces;
using Blog.Infrastructure.Authentication.Services;
using Blog.Infrastructure.Authentication.Services.Interfaces;
using Blog.Infrastructure.Configuration.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Blog.Infrastructure.Authentication
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddAuthenticationDependencies(this IServiceCollection services)
        {
            services.AddJwtDependencies();
            services.AddHttpContextAccessor();

            services.AddScoped<IClaimsFactory, ClaimsFactory>();
            services.AddScoped<IPasswordHashService, PasswordHashService>();
            services.AddScoped<IJwtTokenService, JwtTokenService>();

            services.AddScoped<ICurrentUserDataProvider, CurrentUserDataProvider>();

            return services;
        }

        private static IServiceCollection AddJwtDependencies(this IServiceCollection services)
        {
            var jwtSettings = services.BuildServiceProvider().GetService<IOptions<JwtSettings>>().Value;
            var key = Encoding.ASCII.GetBytes(jwtSettings.Secret);

            services
                .AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            return services;
        }
    }
}