﻿using System.Collections.Generic;
using System.Security.Claims;
using Blog.Infrastructure.Authentication.Constants;

namespace Blog.Infrastructure.Authentication.Domain
{
    public sealed class CurrentUserData
    {
        public int Id { get; }
        public string Email { get; }
        public string FullName { get; }
        public string Role { get; }

        public CurrentUserData(int id, string email, string fullName, string role)
        {
            Id = id;
            Email = email;
            FullName = fullName;
            Role = role;
        }

        public CurrentUserData(IEnumerable<Claim> claims)
        {
            if (claims == null)
                return;

            foreach (var claim in claims)
            {
                switch (claim.Type)
                {
                    case ClaimsConstants.Id:
                        Id = int.Parse(claim.Value);
                        break;
                    case ClaimsConstants.Email:
                        Email = claim.Value;
                        break;
                    case ClaimsConstants.FullName:
                        FullName = claim.Value;
                        break;
                    case ClaimsConstants.Role:
                        Role = claim.Value;
                        break;
                }
            }
        }
    }

}