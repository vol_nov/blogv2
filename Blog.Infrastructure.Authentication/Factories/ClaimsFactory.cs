﻿using System.Collections.Generic;
using Blog.Core.Domain;
using Blog.Infrastructure.Authentication.Constants;
using Blog.Infrastructure.Authentication.Factories.Interfaces;

namespace Blog.Infrastructure.Authentication.Factories
{
    internal sealed class ClaimsFactory : IClaimsFactory
    {
        public IReadOnlyDictionary<string, string> Get(User user)
        {
            return new Dictionary<string, string>
            {
                { ClaimsConstants.Email, user.Email },
                { ClaimsConstants.Id, user.Id.ToString() },
                { ClaimsConstants.FullName, user.FullName },
                { ClaimsConstants.Role, user.Role },
            };
        }
    }
}