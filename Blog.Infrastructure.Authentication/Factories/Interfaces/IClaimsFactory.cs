﻿using System.Collections.Generic;
using Blog.Core.Domain;

namespace Blog.Infrastructure.Authentication.Factories.Interfaces
{
    public interface IClaimsFactory
    {
        IReadOnlyDictionary<string, string> Get(User user);
    }
}