﻿using Blog.Infrastructure.Authentication.Constants;
using Blog.Infrastructure.Authentication.Domain;
using Blog.Infrastructure.Authentication.Providers.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Blog.Infrastructure.Authentication.Providers
{
    internal sealed class CurrentUserDataProvider : ICurrentUserDataProvider
    {
        public CurrentUserData Current { get; }
        
        public CurrentUserDataProvider(IHttpContextAccessor httpContextAccessor)
        {
            Current = Create(httpContextAccessor);
        }

        private CurrentUserData Create(IHttpContextAccessor httpContextAccessor)
        {
            if (!httpContextAccessor.HttpContext.User.HasClaim(x => x.Type == ClaimsConstants.Id))
            {
                return null;
            }

            return new CurrentUserData(httpContextAccessor.HttpContext.User.Claims);
        }
    }
}