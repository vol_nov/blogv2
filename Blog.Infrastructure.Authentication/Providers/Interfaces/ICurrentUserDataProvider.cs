﻿using Blog.Infrastructure.Authentication.Domain;

namespace Blog.Infrastructure.Authentication.Providers.Interfaces
{
    public interface ICurrentUserDataProvider
    {
        CurrentUserData Current { get; }
    }
}