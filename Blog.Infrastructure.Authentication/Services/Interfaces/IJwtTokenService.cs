﻿using Blog.Core.Domain;

namespace Blog.Infrastructure.Authentication.Services.Interfaces
{
    public interface IJwtTokenService
    {
        string Create(User user);
    }
}