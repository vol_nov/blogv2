﻿namespace Blog.Infrastructure.Authentication.Services.Interfaces
{
    public interface IPasswordHashService
    {
        string CalculateHash(string password);
    }
}