﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Authentication.Factories.Interfaces;
using Blog.Infrastructure.Authentication.Services.Interfaces;
using Blog.Infrastructure.Configuration.Settings;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Blog.Infrastructure.Authentication.Services
{
    internal sealed class JwtTokenService : IJwtTokenService
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IClaimsFactory _claimsFactory;
        private readonly JwtSettings _jwtSettings;

        public JwtTokenService(
            IDateTimeProvider dateTimeProvider,
            IClaimsFactory claimsFactory,
            IOptions<JwtSettings> jwtSettingsOptions)
        {
            _dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            _claimsFactory = claimsFactory ?? throw new ArgumentNullException(nameof(claimsFactory));
            _jwtSettings = jwtSettingsOptions.Value ?? throw new ArgumentNullException(nameof(jwtSettingsOptions));
        }

        public string Create(User user)
        {
            var claims = _claimsFactory.Get(user);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims.Select(x => new Claim(x.Key, x.Value)).ToArray()),
                Expires = _dateTimeProvider.Current.AddDays(_jwtSettings.ExpirationPeriodInDays).DateTime,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}