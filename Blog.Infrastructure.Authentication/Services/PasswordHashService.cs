﻿using System;
using System.Security.Cryptography;
using System.Text;
using Blog.Infrastructure.Authentication.Services.Interfaces;

namespace Blog.Infrastructure.Authentication.Services
{
    internal sealed class PasswordHashService : IPasswordHashService
    {
        private const string Salt = "adhasdhasdhas";

        public string CalculateHash(string password)
        {
            using var sha512 = new SHA512CryptoServiceProvider();
            var digest = sha512.ComputeHash(Encoding.UTF8.GetBytes(password + Salt));
            var base64digest = Convert.ToBase64String(digest, 0, digest.Length);
            return base64digest.Substring(0, base64digest.Length - 2);
        }
    }
}