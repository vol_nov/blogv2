﻿using Blog.Infrastructure.Configuration.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Blog.Infrastructure.Configuration
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSettingsDependencies(
            this IServiceCollection services,
            IConfiguration configuration)
        {
            services.Configure<SqlServerSettings>(configuration.GetSection(nameof(SqlServerSettings)));
            services.Configure<BlobStorageSettings>(configuration.GetSection(nameof(BlobStorageSettings)));
            services.Configure<JwtSettings>(configuration.GetSection(nameof(JwtSettings)));
            services.Configure<HealthChecksSettings>(configuration.GetSection(nameof(HealthChecksSettings)));

            return services;
        }
    }
}