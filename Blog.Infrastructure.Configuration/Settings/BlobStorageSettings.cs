﻿namespace Blog.Infrastructure.Configuration.Settings
{
    public sealed class BlobStorageSettings
    {
        public string ConnectionString { get; set; }
        public string FilesContainerName { get; set; }
    }
}