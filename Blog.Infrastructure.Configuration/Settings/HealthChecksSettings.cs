﻿namespace Blog.Infrastructure.Configuration.Settings
{
    public sealed class HealthChecksSettings
    {
        public int MigrationNumber { get; set; }
    }
}
