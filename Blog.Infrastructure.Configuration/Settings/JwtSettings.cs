﻿namespace Blog.Infrastructure.Configuration.Settings
{
    public sealed class JwtSettings
    {
        public string Secret { get; set; }
        public int ExpirationPeriodInDays { get; set; }
    }
}