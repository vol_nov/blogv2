﻿namespace Blog.Infrastructure.Configuration.Settings
{
    public sealed class SqlServerSettings
    {
        public string ConnectionString { get; set; }
    }
}