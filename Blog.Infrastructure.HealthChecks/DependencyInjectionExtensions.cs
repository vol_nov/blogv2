﻿using Microsoft.Extensions.DependencyInjection;

namespace Blog.Infrastructure.HealthChecks
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddHealthCheckDependencies(this IServiceCollection services)
        {
            services
                .AddHealthChecks()
                .AddTypeActivatedCheck<SqlServerHealthCheck>(HealthCheckNameConstants.SqlServerAvailability)
                .AddTypeActivatedCheck<SqlServerMigrationsHealthCheck>(HealthCheckNameConstants.SqlServerMigrations);

            return services;
        }
    }
}
