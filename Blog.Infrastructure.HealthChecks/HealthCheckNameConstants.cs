﻿namespace Blog.Infrastructure.HealthChecks
{
    internal static class HealthCheckNameConstants
    {
        public const string SqlServerAvailability = "sqlServerAvailability";
        public const string SqlServerMigrations = "sqlServerMigrations";
    }
}
