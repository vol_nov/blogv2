﻿using Blog.Infrastructure.Configuration.Settings;
using Dapper;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace Blog.Infrastructure.HealthChecks
{
    internal sealed class SqlServerHealthCheck : IHealthCheck
    {
        private readonly SqlServerSettings _sqlServerSettings;

        public SqlServerHealthCheck(IOptions<SqlServerSettings> sqlServerSettingsOptions)
        {
            _sqlServerSettings = sqlServerSettingsOptions.Value;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                using var sqlConnection = new SqlConnection(_sqlServerSettings.ConnectionString);
                await sqlConnection.QueryAsync("SELECT TOP(1) * FROM [dbo].[Countries]");
                return HealthCheckResult.Healthy();
            }
            catch (Exception e)
            {
                return HealthCheckResult.Unhealthy("SqlServerHealthCheck failed", e);
            }
        }
    }
}
