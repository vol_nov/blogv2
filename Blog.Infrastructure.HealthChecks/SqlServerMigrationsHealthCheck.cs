﻿using Blog.Infrastructure.Configuration.Settings;
using Dapper;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace Blog.Infrastructure.HealthChecks
{
    internal sealed class SqlServerMigrationsHealthCheck : IHealthCheck
    {
        private readonly SqlServerSettings _sqlServerSettings;
        private readonly HealthChecksSettings _healthChecksSettings;

        public SqlServerMigrationsHealthCheck(IOptions<SqlServerSettings> sqlServerSettingsOptions, IOptions<HealthChecksSettings> healthChecksSettingsOptions)
        {
            _sqlServerSettings = sqlServerSettingsOptions.Value;
            _healthChecksSettings = healthChecksSettingsOptions.Value;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            try
            {
                using var sqlConnection = new SqlConnection(_sqlServerSettings.ConnectionString);
                var version = await sqlConnection.QuerySingleAsync<int>("SELECT TOP(1) [Version] FROM [dbo].[VersionInfo] ORDER BY [Version] DESC");
                return version == _healthChecksSettings.MigrationNumber
                    ? HealthCheckResult.Healthy()
                    : HealthCheckResult.Degraded();
            }
            catch (Exception e)
            {
                return HealthCheckResult.Unhealthy("SqlServerMigrationsHealthCheck failed", e);
            }
        }
    }
}
