﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Blog.Infrastructure.Logging
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddLoggingDependencies(this IServiceCollection services)
        {
            return services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
        }
    }
}
