﻿using Blob.Infrastructure.BlobStorage.Repositories;
using Blog.Core.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Blob.Infrastructure.Persistence.BlobStorage
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddBlobStorageDependencies(this IServiceCollection services)
        {
            services.AddScoped<IAttachmentContentRepository, AttachmentContentRepository>();

            return services;
        }
    }
}