﻿using System;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Configuration.Settings;
using Microsoft.Extensions.Options;

namespace Blob.Infrastructure.BlobStorage.Repositories
{
    internal sealed class AttachmentContentRepository : IAttachmentContentRepository
    {
        private readonly BlobContainerClient _blobContainerClient;

        public AttachmentContentRepository(IOptions<BlobStorageSettings> blobStorageSettingsOptions)
        {
            _blobContainerClient = new BlobContainerClient(
                blobStorageSettingsOptions.Value.ConnectionString,
                blobStorageSettingsOptions.Value.FilesContainerName);
        }

        public async Task AddContent(Guid attachmentId, Stream content)
        {
            var blobClient = _blobContainerClient.GetBlobClient($"attachments/{attachmentId}");
            await blobClient.UploadAsync(content);
        }

        public async Task<Stream> GetContent(Guid attachmentId)
        {
            var blobClient = _blobContainerClient.GetBlobClient($"attachments/{attachmentId}");
            var response = await blobClient.DownloadContentAsync();
            return response.Value.Content.ToStream();
        }
    }
}