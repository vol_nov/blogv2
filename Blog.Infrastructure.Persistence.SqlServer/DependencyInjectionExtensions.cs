﻿using System;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Orm;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Repositories;
using MicroOrm.Dapper.Repositories.Config;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Microsoft.Extensions.DependencyInjection;

namespace Blog.Infrastructure.Persistence.SqlServer
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSqlServerPersistenceDependencies(this IServiceCollection services)
        {
            services.AddMicroOrmDependencies();
            services.AddRepositories();
            services.AddMappers();

            return services;
        }

        private static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IAttachmentRepository, AttachmentRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IBlogPostLikeRepository, BlogPostLikeRepository>();
            services.AddScoped<IBlogPostCommentRepository, BlogPostCommentRepository>();
            services.AddScoped<IBlogPostRepository, BlogPostRepository>();

            return services;
        }

        private static IServiceCollection AddMappers(this IServiceCollection services)
        {
            services.AddScoped<IUserEntityMapper, UserEntityMapper>();
            services.AddScoped<ICountryEntityMapper, CountryEntityMapper>();
            services.AddScoped<IBlogPostCommentEntityMapper, BlogPostCommentEntityMapper>();
            services.AddScoped<IBlogPostEntityMapper, BlogPostEntityMapper>();

            return services;
        }

        private static IServiceCollection AddMicroOrmDependencies(this IServiceCollection services)
        {
            MicroOrmConfig.SqlProvider = SqlProvider.MSSQL;

            services.AddSingleton(typeof(ISqlGenerator<>), typeof(CustomSqlGenerator<>));
            services.AddSingleton<ISqlDbConnectionFactory, SqlDbConnectionFactory>();

            return services;
        }
    }
}