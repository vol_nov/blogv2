﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace Blog.Infrastructure.Persistence.SqlServer.Entities
{
    [Table("Attachments")]
    internal sealed class AttachmentEntity
    {
        [Key, Identity]
        public Guid AttachmentId { get; set; }
        public Guid BlogPostId { get; set; }
        public string OriginalFileName { get; set; }

        [UpdatedAt]
        public DateTimeOffset CreatedDate { get; set; }
    }
}