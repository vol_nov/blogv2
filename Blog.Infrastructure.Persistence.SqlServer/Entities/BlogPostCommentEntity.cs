﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace Blog.Infrastructure.Persistence.SqlServer.Entities
{
    [Table("BlogPostComments")]
    internal sealed class BlogPostCommentEntity
    {
        [Key, Identity]
        public Guid BlogPostCommentId { get; set; }
        public Guid BlogPostId { get; set; }
        public int UserId { get; set; }
        public string Content { get; set; }

        [UpdatedAt]
        public DateTimeOffset CreatedDate { get; set; }
    }
}