﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.Attributes.Joins;

namespace Blog.Infrastructure.Persistence.SqlServer.Entities
{
    [Table("BlogPosts")]
    internal sealed class BlogPostEntity
    {
        [Key, Identity]
        public Guid BlogPostId { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }

        [UpdatedAt]
        public DateTimeOffset CreatedDate { get; set; }

        public int AuthorId { get; set; }

        [LeftJoin("BlogPostComments", "BlogPostId", "BlogPostId")]
        public List<BlogPostCommentEntity> Comments { get; set; }

        [LeftJoin("BlogPostLikes", "BlogPostId", "BlogPostId")]
        public List<BlogPostLikeEntity> Likes { get; set; }
    }
}