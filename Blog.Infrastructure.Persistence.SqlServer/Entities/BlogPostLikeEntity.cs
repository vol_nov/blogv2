﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace Blog.Infrastructure.Persistence.SqlServer.Entities
{
    [Table("BlogPostLikes")]
    internal sealed class BlogPostLikeEntity
    {
        [Key, Identity]
        public Guid BlogPostLikeId {  get; set; }
        public Guid BlogPostId {  get; set; }
        public int UserId {  get; set; }
    }
}