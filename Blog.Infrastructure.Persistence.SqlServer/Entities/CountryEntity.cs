﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace Blog.Infrastructure.Persistence.SqlServer.Entities
{
    [Table("Countries")]
    internal sealed class CountryEntity
    {
        [Key, Identity]
        public int CountryId { get; set; }
        public string Name { get; set; }
    }
}