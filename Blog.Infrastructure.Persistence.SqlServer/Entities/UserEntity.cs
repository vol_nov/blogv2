﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MicroOrm.Dapper.Repositories.Attributes;

namespace Blog.Infrastructure.Persistence.SqlServer.Entities
{
    [Table("Users")]
    internal sealed class UserEntity
    {
        [Key, Identity]
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string FullName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int CountryId { get; set; }
    }
}