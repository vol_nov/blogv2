﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using MicroOrm.Dapper.Repositories;

namespace Blog.Infrastructure.Persistence.SqlServer.Extensions
{
    internal static class DapperRepositoryExtensions
    {
        public static bool Insert<TEntity, TIdentity>(this DapperRepository<TEntity> dapperRepository, TEntity instance)
            where TEntity : class
        {
            var insert = dapperRepository.SqlGenerator.GetInsert(instance);
            if (dapperRepository.SqlGenerator.IsIdentity)
            {
                var newId = dapperRepository.Connection.Query<TIdentity>(insert.GetSql(), insert.Param).FirstOrDefault<TIdentity>();
                dapperRepository.SqlGenerator.IdentitySqlProperty.PropertyInfo.SetValue((object) instance, newId);
                return !newId.Equals(default(TIdentity));
            }

            return dapperRepository.Connection.Execute(insert.GetSql(), (object) instance) > 0;
        }

        public static async Task<bool> InsertAsync<TEntity, TIdentity>(this DapperRepository<TEntity> dapperRepository, TEntity instance)
            where TEntity : class
        {
            var insert = dapperRepository.SqlGenerator.GetInsert(instance);
            if (dapperRepository.SqlGenerator.IsIdentity)
            {
                var newId = (await dapperRepository.Connection.QueryAsync<TIdentity>(insert.GetSql(), insert.Param)).FirstOrDefault<TIdentity>();
                dapperRepository.SqlGenerator.IdentitySqlProperty.PropertyInfo.SetValue((object) instance, newId);
                return !newId.Equals(default(TIdentity));
            }

            return await dapperRepository.Connection.ExecuteAsync(insert.GetSql(), (object) instance) > 0;
        }
 
        public static bool Insert<TEntity, TIdentity>(this DapperRepository<TEntity> dapperRepository, TEntity instance, IDbTransaction transaction)
            where TEntity : class
        {
            var insert = dapperRepository.SqlGenerator.GetInsert(instance);
            if (dapperRepository.SqlGenerator.IsIdentity)
            {
                var newId = dapperRepository.Connection.Query<TIdentity>(insert.GetSql(), insert.Param, transaction).FirstOrDefault<TIdentity>();
                dapperRepository.SqlGenerator.IdentitySqlProperty.PropertyInfo.SetValue((object) instance, newId);
                return !newId.Equals(default(TIdentity));
            }

            return dapperRepository.Connection.Execute(insert.GetSql(), (object) instance, transaction) > 0;
        }

        public static async Task<bool> InsertAsync<TEntity, TIdentity>(this DapperRepository<TEntity> dapperRepository, TEntity instance, IDbTransaction transaction)
            where TEntity : class
        {
            var insert = dapperRepository.SqlGenerator.GetInsert(instance);
            if (dapperRepository.SqlGenerator.IsIdentity)
            {
                var newId = (await dapperRepository.Connection.QueryAsync<TIdentity>(insert.GetSql(), insert.Param, transaction)).FirstOrDefault<TIdentity>();
                dapperRepository.SqlGenerator.IdentitySqlProperty.PropertyInfo.SetValue((object) instance, newId);
                return !newId.Equals(default(TIdentity));
            }

            return await dapperRepository.Connection.ExecuteAsync(insert.GetSql(), (object) instance, transaction) > 0;
        }
    }
}