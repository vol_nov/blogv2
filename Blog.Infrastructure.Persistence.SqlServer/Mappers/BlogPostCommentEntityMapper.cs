﻿using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers
{
    internal sealed class BlogPostCommentEntityMapper : IBlogPostCommentEntityMapper
    {
        public BlogPostCommentEntity MapToEntity(BlogPostComment comment)
        {
            return new BlogPostCommentEntity
            {
                BlogPostId = comment.BlogPostId,
                UserId = comment.UserId,
                Content = comment.Content,
                CreatedDate = comment.CreatedDate,
            };
        }

        public BlogPostComment MapToDomain(BlogPostCommentEntity entity)
        {
            return new BlogPostComment
            {
                BlogPostCommentId = entity.BlogPostCommentId,
                BlogPostId = entity.BlogPostId,
                Content = entity.Content,
                CreatedDate = entity.CreatedDate,
                UserId = entity.UserId,
            };
        }
    }
}