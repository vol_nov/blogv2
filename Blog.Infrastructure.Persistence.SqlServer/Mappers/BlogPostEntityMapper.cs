﻿using System.Linq;
using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers
{
    internal sealed class BlogPostEntityMapper : IBlogPostEntityMapper
    {
        public BlogPostEntity MapToEntity(BlogPost blogPost)
        {
            return new BlogPostEntity
            {
                BlogPostId = blogPost.BlogPostId,
                Header = blogPost.Header,
                Description = blogPost.Description,
                AuthorId = blogPost.AuthorId,
                CreatedDate = blogPost.CreatedDate,
            };
        }

        public BlogPost MapToDomain(BlogPostEntity blogPostEntity, int currentUserId)
        {
            return new BlogPost
            {
                BlogPostId = blogPostEntity.BlogPostId,
                Header = blogPostEntity.Header,
                Description = blogPostEntity.Description,
                CreatedDate = blogPostEntity.CreatedDate,
                AuthorId = blogPostEntity.AuthorId,
                LikesCount = blogPostEntity.Likes?.Count ?? 0,
                IsLikedByUser = blogPostEntity.Likes?.Any(x => x.UserId == currentUserId) ?? false,
            };
        }
    }
}