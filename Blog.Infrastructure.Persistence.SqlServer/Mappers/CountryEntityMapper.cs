﻿using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers
{
    internal sealed class CountryEntityMapper : ICountryEntityMapper
    {
        public Country MapToDomain(CountryEntity countryEntity)
        {
            return new Country
            {
                CountryId = countryEntity.CountryId,
                Name = countryEntity.Name,
            };
        }
    }
}