﻿using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces
{
    internal interface IBlogPostCommentEntityMapper
    {
        BlogPostCommentEntity MapToEntity(BlogPostComment comment);
        BlogPostComment MapToDomain(BlogPostCommentEntity entity);
    }
}