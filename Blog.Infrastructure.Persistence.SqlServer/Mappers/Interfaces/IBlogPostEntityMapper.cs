﻿using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces
{
    internal interface IBlogPostEntityMapper
    {
        BlogPostEntity MapToEntity(BlogPost blogPost);
        BlogPost MapToDomain(BlogPostEntity blogPostEntity, int currentUserId);
    }
}