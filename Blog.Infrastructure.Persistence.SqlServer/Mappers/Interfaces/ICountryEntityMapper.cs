﻿using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces
{
    internal interface ICountryEntityMapper
    {
        Country MapToDomain(CountryEntity countryEntity);
    }
}