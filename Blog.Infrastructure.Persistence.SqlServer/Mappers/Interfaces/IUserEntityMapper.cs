﻿using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces
{
    internal interface IUserEntityMapper
    {
        UserEntity MapToEntity(User user);
        User MapToDomain(UserEntity user);
    }
}