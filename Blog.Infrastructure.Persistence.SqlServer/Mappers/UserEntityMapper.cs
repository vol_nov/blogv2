﻿using Blog.Core.Domain;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;

namespace Blog.Infrastructure.Persistence.SqlServer.Mappers
{
    internal sealed class UserEntityMapper : IUserEntityMapper
    {
        public UserEntity MapToEntity(User user)
        {
            if (user == null)
                return null;

            return new UserEntity
            {
                Email = user.Email,
                Username = user.UserName,
                FullName = user.FullName,
                PasswordHash = user.PasswordHash,
                DateOfBirth = user.DateOfBirth,
                CountryId = user.CountryId,
            };
        }

        public User MapToDomain(UserEntity user)
        {
            if (user == null)
                return null;

            return new User
            {
                Id = user.UserId,
                Email = user.Email,
                UserName = user.Username,
                FullName = user.FullName,
                PasswordHash = user.PasswordHash,
                DateOfBirth = user.DateOfBirth,
                CountryId = user.CountryId,
                Role = "User"
            };
        }
    }
}