﻿using System;
using System.Linq;
using System.Reflection;
using MicroOrm.Dapper.Repositories.Attributes;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace Blog.Infrastructure.Persistence.SqlServer.Orm
{
    public class CustomSqlGenerator<T> : SqlGenerator<T> where T : class
    {
        public override SqlQuery GetInsert(T entity)
        {
            if (Provider != SqlProvider.MSSQL)
                return base.GetInsert(entity);

            var list = (IsIdentity
                ? SqlProperties.Where(
                    (Func<SqlPropertyMetadata, bool>) (p =>
                        !p.PropertyName.Equals(IdentitySqlProperty.PropertyName,
                            StringComparison.OrdinalIgnoreCase)))
                : SqlProperties).ToList();
            if (HasUpdatedAt)
            {
                var customAttribute = UpdatedAtProperty.GetCustomAttribute<UpdatedAtAttribute>();
                var dateTimeOffset = customAttribute.TimeKind == DateTimeKind.Local
                    ? new DateTimeOffset(DateTime.Now)
                    : new DateTimeOffset(DateTime.UtcNow);
                if (customAttribute.OffSet != 0)
                    dateTimeOffset = dateTimeOffset.ToOffset(TimeSpan.FromHours(customAttribute.OffSet));
                UpdatedAtProperty.SetValue(entity, dateTimeOffset.DateTime);
            }

            var sqlQuery = new SqlQuery(entity);
            sqlQuery.SqlBuilder.AppendFormat(
                "INSERT INTO {0} ({1}) {2} VALUES ({3})",
                TableName,
                string.Join(", ", list.Select((Func<SqlPropertyMetadata, string>) (p => p.ColumnName))),
                AddOutputInserted(),
                string.Join(", ", list.Select((Func<SqlPropertyMetadata, string>) (p => "@" + p.PropertyName))));

            if (IsIdentity)
            {
                sqlQuery.SqlBuilder.Append(" SELECT SCOPE_IDENTITY() AS " + IdentitySqlProperty.ColumnName);
            }

            return sqlQuery;
        }

        private string AddOutputInserted()
        {
            if (!IsIdentity)
                return string.Empty;
            return $" OUTPUT inserted.{IdentitySqlProperty.ColumnName}";
        }
    }
}