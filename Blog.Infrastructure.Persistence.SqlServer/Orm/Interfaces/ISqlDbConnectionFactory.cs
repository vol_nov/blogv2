﻿using System.Data;

namespace Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces
{
    public interface ISqlDbConnectionFactory
    {
        IDbConnection CreateConnection();
    }
}