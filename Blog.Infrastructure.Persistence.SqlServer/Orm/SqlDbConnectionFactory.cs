﻿using System.Data;
using System.Data.SqlClient;
using Blog.Infrastructure.Configuration.Settings;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using Microsoft.Extensions.Options;

namespace Blog.Infrastructure.Persistence.SqlServer.Orm
{
    internal sealed class SqlDbConnectionFactory : ISqlDbConnectionFactory
    {
        private readonly SqlServerSettings _sqlServerSettings;

        public SqlDbConnectionFactory(IOptions<SqlServerSettings> sqlServerSettingsOptions)
        {
            _sqlServerSettings = sqlServerSettingsOptions.Value;
        }

        public IDbConnection CreateConnection()
        {
            return new SqlConnection(_sqlServerSettings.ConnectionString);
        }
    }
}