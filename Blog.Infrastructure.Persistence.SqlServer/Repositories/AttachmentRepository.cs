﻿using Blog.Core.Commands;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Linq;
using System.Threading.Tasks;
using Blog.Infrastructure.Persistence.SqlServer.Extensions;

namespace Blog.Infrastructure.Persistence.SqlServer.Repositories
{
    internal sealed class AttachmentRepository : DapperRepository<AttachmentEntity>, IAttachmentRepository
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public AttachmentRepository(
            ISqlDbConnectionFactory sqlDbConnectionFactory,
            IDateTimeProvider dateTimeProvider)
            : base(sqlDbConnectionFactory.CreateConnection())
        {
            _dateTimeProvider = dateTimeProvider;
        }

        public AttachmentRepository(
            ISqlDbConnectionFactory sqlDbConnectionFactory,
            ISqlGenerator<AttachmentEntity> sqlGenerator,
            IDateTimeProvider dateTimeProvider)
            : base(sqlDbConnectionFactory.CreateConnection(), sqlGenerator)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<bool> Exists(Guid attachmentId)
        {
            var attachments = await this.FindAllAsync(x => x.AttachmentId == attachmentId);
            return attachments.Any();
        }

        public async Task<Guid> AddAttachment(Guid blogPostId, AttachedFile file)
        {
            var currentDateTime = _dateTimeProvider.Current;

            var attachment = new AttachmentEntity
            {
                BlogPostId = blogPostId,
                OriginalFileName = file.FileName,
                CreatedDate = currentDateTime
            };

            await this.InsertAsync<AttachmentEntity, Guid>(attachment);
            return attachment.AttachmentId;
        }
    }
}
