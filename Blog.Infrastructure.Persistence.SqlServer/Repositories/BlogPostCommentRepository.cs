﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Extensions;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace Blog.Infrastructure.Persistence.SqlServer.Repositories
{
    internal sealed class BlogPostCommentRepository : DapperRepository<BlogPostCommentEntity>, IBlogPostCommentRepository
    {
        private readonly IBlogPostCommentEntityMapper _blogPostCommentEntityMapper;
        public BlogPostCommentRepository(
            ISqlDbConnectionFactory sqlDbConnectionFactory,
            IBlogPostCommentEntityMapper blogPostCommentEntityMapper)
            : base(sqlDbConnectionFactory.CreateConnection())
        {
            _blogPostCommentEntityMapper = blogPostCommentEntityMapper;
        }

        public BlogPostCommentRepository(
            ISqlDbConnectionFactory sqlDbConnectionFactory,
            ISqlGenerator<BlogPostCommentEntity> sqlGenerator,
            IBlogPostCommentEntityMapper blogPostCommentEntityMapper)
            : base(sqlDbConnectionFactory.CreateConnection(), sqlGenerator)
        {
            _blogPostCommentEntityMapper = blogPostCommentEntityMapper;
        }

        public async Task<Guid> Add(BlogPostComment comment)
        {
            var entity = _blogPostCommentEntityMapper.MapToEntity(comment);
            await this.InsertAsync<BlogPostCommentEntity, Guid>(entity);
            return entity.BlogPostCommentId;
        }

        public async Task<IReadOnlyCollection<BlogPostComment>> GetComments(Guid blogPostId, int page, int size)
        {
            var result = await SetLimit((uint) size, (uint) ((page -1) * size))
                .FindAllAsync(x => x.BlogPostId == blogPostId);

            return result.Select(x => _blogPostCommentEntityMapper.MapToDomain(x)).ToList();
        }
    }
}