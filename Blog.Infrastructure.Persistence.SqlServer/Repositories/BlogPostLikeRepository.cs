﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Extensions;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace Blog.Infrastructure.Persistence.SqlServer.Repositories
{
    internal sealed class BlogPostLikeRepository : DapperRepository<BlogPostLikeEntity>, IBlogPostLikeRepository
    {
        public BlogPostLikeRepository(ISqlDbConnectionFactory sqlDbConnectionFactory)
            : base(sqlDbConnectionFactory.CreateConnection())
        {
        }

        public BlogPostLikeRepository(ISqlDbConnectionFactory sqlDbConnectionFactory, ISqlGenerator<BlogPostLikeEntity> sqlGenerator)
            : base(sqlDbConnectionFactory.CreateConnection(), sqlGenerator)
        {
        }

        public async Task<bool> Like(Guid blogPostId, int userId)
        {
            this.Connection.Open();
            var dbTransaction = this.Connection.BeginTransaction(IsolationLevel.ReadCommitted);

            try
            {
                var likeEntity = (await FindAllAsync(x => x.BlogPostId == blogPostId && x.UserId == userId, dbTransaction))
                    .SingleOrDefault();

                var isLikeAdded = likeEntity == null;

                if (isLikeAdded)
                {
                    likeEntity = new BlogPostLikeEntity
                    {
                        BlogPostId = blogPostId,
                        UserId = userId,
                    };

                    await this.InsertAsync<BlogPostLikeEntity, Guid>(likeEntity, dbTransaction);
                }
                else
                {
                    await this.DeleteAsync(likeEntity, dbTransaction);
                }

                dbTransaction.Commit();
                return isLikeAdded;
            }
            catch (Exception)
            {
                dbTransaction.Rollback();
                throw;
            }
            finally
            {
                dbTransaction.Dispose();
            }
        }
    }
}