﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using Blog.Infrastructure.Persistence.SqlServer.Extensions;

namespace Blog.Infrastructure.Persistence.SqlServer.Repositories
{
    internal sealed class BlogPostRepository : DapperRepository<BlogPostEntity>, IBlogPostRepository
    {
        private readonly IBlogPostEntityMapper _blogPostEntityMapper;

        public BlogPostRepository(
            ISqlDbConnectionFactory sqlDbConnectionFactory,
            IBlogPostEntityMapper blogPostEntityMapper)
            : base(sqlDbConnectionFactory.CreateConnection())
        {
            _blogPostEntityMapper = blogPostEntityMapper;
        }

        public BlogPostRepository(
            ISqlDbConnectionFactory sqlDbConnectionFactory,
            ISqlGenerator<BlogPostEntity> sqlGenerator,
            IBlogPostEntityMapper blogPostEntityMapper)
            : base(sqlDbConnectionFactory.CreateConnection(), sqlGenerator)
        {
            _blogPostEntityMapper = blogPostEntityMapper;
        }

        public async Task<Guid> Add(BlogPost blogPost)
        {
            var entity = _blogPostEntityMapper.MapToEntity(blogPost);
            await this.InsertAsync<BlogPostEntity, Guid>(entity);
            return entity.BlogPostId;
        }

        public async Task<IReadOnlyCollection<BlogPost>> GetBlogPosts(int currentUserId, int targetUserId)
        {
            var blogPosts = (await FindAllAsync<BlogPostCommentEntity, BlogPostLikeEntity>(
                x => x.AuthorId == targetUserId,
                x => x.Comments,
                x => x.Likes))
                .ToList();
            return blogPosts.Select(x => _blogPostEntityMapper.MapToDomain(x, currentUserId)).ToList();
        }

        public async Task<bool> ContainsBlogPostWithId(Guid blogPostId)
        {
            return (await FindAllAsync(x => x.BlogPostId == blogPostId)).Any();
        }
    }
}