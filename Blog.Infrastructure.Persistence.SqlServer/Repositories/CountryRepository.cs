﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace Blog.Infrastructure.Persistence.SqlServer.Repositories
{
    internal sealed class CountryRepository : DapperRepository<CountryEntity>, ICountryRepository
    {
        private readonly ICountryEntityMapper _countryEntityMapper;

        public CountryRepository(
            ICountryEntityMapper countryEntityMapper,
            ISqlDbConnectionFactory sqlDbConnectionFactory)
            : base(sqlDbConnectionFactory.CreateConnection())
        {
            _countryEntityMapper = countryEntityMapper;
        }

        public CountryRepository(
            ICountryEntityMapper countryEntityMapper,
            ISqlDbConnectionFactory sqlDbConnectionFactory,
            ISqlGenerator<CountryEntity> sqlGenerator)
            : base(sqlDbConnectionFactory.CreateConnection(), sqlGenerator)
        {
            _countryEntityMapper = countryEntityMapper;
        }

        public async Task<bool> IsValidCountryId(int countryId)
        {
            return (await FindAllAsync(x => x.CountryId == countryId)).Any();
        }

        public async Task<IReadOnlyCollection<Country>> GetCountries()
        {
            return (await FindAllAsync())
                .Select(_countryEntityMapper.MapToDomain)
                .ToList();
        }
    }
}