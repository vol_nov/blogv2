﻿using System.Linq;
using System.Threading.Tasks;
using Blog.Core.Domain;
using Blog.Core.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Entities;
using Blog.Infrastructure.Persistence.SqlServer.Mappers.Interfaces;
using Blog.Infrastructure.Persistence.SqlServer.Orm.Interfaces;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;

namespace Blog.Infrastructure.Persistence.SqlServer.Repositories
{
    internal sealed class UserRepository : DapperRepository<UserEntity>, IUserRepository
    {
        private readonly IUserEntityMapper _userEntityMapper;

        public UserRepository(IUserEntityMapper userEntityMapper, ISqlDbConnectionFactory sqlDbConnectionFactory)
            : base(sqlDbConnectionFactory.CreateConnection())
        {
            _userEntityMapper = userEntityMapper;
        }

        public UserRepository(IUserEntityMapper userEntityMapper, ISqlDbConnectionFactory sqlDbConnectionFactory, ISqlGenerator<UserEntity> sqlGenerator)
            : base(sqlDbConnectionFactory.CreateConnection(), sqlGenerator)
        {
            _userEntityMapper = userEntityMapper;
        }

        public async Task<bool> ContainsUserWithId(int userId)
        {
            return (await FindAllAsync(x => x.UserId == userId)).Any();
        }

        public async Task<bool> ContainsUserWithEmail(string email)
        {
            return (await FindAllAsync(x => x.Email == email)).Any();
        }

        public async Task<bool> ContainsUserWithUsername(string userName)
        {
            return (await FindAllAsync(x => x.Username == userName)).Any();
        }

        public async Task<User> GetUserByEmail(string email)
        {
            var userEntity = (await FindAllAsync(x => x.Email == email)).SingleOrDefault();
            return _userEntityMapper.MapToDomain(userEntity);
        }

        public async Task Add(User user)
        {
            var userEntity = _userEntityMapper.MapToEntity(user);
            await InsertAsync(userEntity);
        }
    }
}