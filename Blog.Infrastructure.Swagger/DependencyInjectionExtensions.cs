﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Blog.Infrastructure.Swagger
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services, string authenticationScheme)
        {
            return services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new OpenApiInfo { Title = "Blog", Version = "v1" });

                var securitySchema = new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.Http,
                    Scheme = "bearer",
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = authenticationScheme
                    }
                };

                x.AddSecurityDefinition(authenticationScheme, securitySchema);

                var securityRequirement = new OpenApiSecurityRequirement
                {
                    { securitySchema, new[] { authenticationScheme } }
                };

                x.AddSecurityRequirement(securityRequirement);
            }); ;
        }
    }
}
